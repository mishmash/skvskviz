﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Reflection;
using System.Net;
using System.Net.Sockets;
using Telerik.WinControls.UI;
using System.Drawing;
using System.Drawing.Imaging;
using System.Windows.Forms;
using System.IO;

namespace Softech.Softime
{
    public static class Extensions
    {
        public static string GetAssemblyTitle()
        {
            Assembly assembly = Assembly.GetExecutingAssembly();
            string result = string.Empty;
            object[] attributes = assembly.GetCustomAttributes(typeof(AssemblyTitleAttribute), false);
            if (attributes.Length == 1)
            {
                result = ((AssemblyTitleAttribute)attributes[0]).Title;
            }
            return result;
        }

        public static string GetIPAddress()
        {
            IPHostEntry host;
            string localIP = string.Empty;
            host = Dns.GetHostEntry(Dns.GetHostName());
            foreach (IPAddress ip in host.AddressList)
            {
                if (ip.AddressFamily == AddressFamily.InterNetwork)
                {
                    localIP = ip.ToString();
                }
            }
            return localIP;
        }

        /// <summary>
        /// Dobi stolpec in ga pretvori v specificen tip stolpca
        /// </summary>
        /// <typeparam name="T">Tip stolpca</typeparam>
        /// <param name="gv">RadGridView</param>
        /// <param name="name">Ime stolpca</param>
        /// <returns></returns>
        public static T GetColumn<T>(this RadGridView gv, string name) where T : GridViewDataColumn
        {
            return (T)gv.Columns.Where(c => c.Name == name).SingleOrDefault();
        }

        public static Image SetImageOpacity(this Image image, float opacity)
        {
            try
            {
                Bitmap bmp = new Bitmap(image.Width, image.Height);
                using (Graphics gfx = Graphics.FromImage(bmp))
                {
                    ColorMatrix matrix = new ColorMatrix();
                    matrix.Matrix33 = opacity;
                    ImageAttributes attributes = new ImageAttributes();
                    attributes.SetColorMatrix(matrix, ColorMatrixFlag.Default, ColorAdjustType.Bitmap);
                    gfx.DrawImage(image, new Rectangle(0, 0, bmp.Width, bmp.Height), 0, 0, image.Width, image.Height, GraphicsUnit.Pixel, attributes);
                }
                return bmp;
            }
            catch (Exception)
            {
                return image;
            }
        }

        public static bool InstallStoredProcedures(Datalayer db)
        {
            if (db.StoredProceduresInstalled())
                return true;

            string sqlCommand = string.Empty;

            using (Stream stream = Assembly.GetCallingAssembly().GetManifestResourceStream("Softech.Softime.Resources.SofTimeSQL.sql"))
            using (StreamReader reader = new StreamReader(stream))
            {
                sqlCommand = reader.ReadToEnd();
            }
            return db.ExecuteSql(sqlCommand);
        }

        public static bool IsSpInstalled(string database)
        {
            var collection = Properties.Settings.Default.spInstalled;
            if (collection.Contains(database))
                return true;
            return false;
        }

        public static void AddSpInstalled(string database)
        {
            var collection = Properties.Settings.Default.spInstalled;
            if (collection.Contains(database))
                return;
            collection.Add(database);
        }
    }
}
