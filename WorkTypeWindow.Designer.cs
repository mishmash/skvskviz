﻿namespace Softech.Softime
{
    partial class WorkTypeWindow
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            Telerik.WinControls.UI.GridViewTextBoxColumn gridViewTextBoxColumn1 = new Telerik.WinControls.UI.GridViewTextBoxColumn();
            Telerik.WinControls.UI.GridViewTextBoxColumn gridViewTextBoxColumn2 = new Telerik.WinControls.UI.GridViewTextBoxColumn();
            Telerik.WinControls.UI.GridViewTextBoxColumn gridViewTextBoxColumn3 = new Telerik.WinControls.UI.GridViewTextBoxColumn();
            Telerik.WinControls.UI.GridViewCheckBoxColumn gridViewCheckBoxColumn1 = new Telerik.WinControls.UI.GridViewCheckBoxColumn();
            Telerik.WinControls.UI.GridViewCheckBoxColumn gridViewCheckBoxColumn2 = new Telerik.WinControls.UI.GridViewCheckBoxColumn();
            Telerik.WinControls.UI.GridViewMultiComboBoxColumn gridViewMultiComboBoxColumn1 = new Telerik.WinControls.UI.GridViewMultiComboBoxColumn();
            System.ComponentModel.ComponentResourceManager resources = new System.ComponentModel.ComponentResourceManager(typeof(WorkTypeWindow));
            this.splitContainer1 = new System.Windows.Forms.SplitContainer();
            this.gridWorktype = new Telerik.WinControls.UI.RadGridView();
            this.btnSave = new System.Windows.Forms.Button();
            this.btnDeleteEntry = new System.Windows.Forms.Button();
            this.btnNewEntry = new System.Windows.Forms.Button();
            ((System.ComponentModel.ISupportInitialize)(this.splitContainer1)).BeginInit();
            this.splitContainer1.Panel1.SuspendLayout();
            this.splitContainer1.Panel2.SuspendLayout();
            this.splitContainer1.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.gridWorktype)).BeginInit();
            this.SuspendLayout();
            // 
            // splitContainer1
            // 
            this.splitContainer1.Dock = System.Windows.Forms.DockStyle.Fill;
            this.splitContainer1.Location = new System.Drawing.Point(0, 0);
            this.splitContainer1.Name = "splitContainer1";
            this.splitContainer1.Orientation = System.Windows.Forms.Orientation.Horizontal;
            // 
            // splitContainer1.Panel1
            // 
            this.splitContainer1.Panel1.Controls.Add(this.gridWorktype);
            // 
            // splitContainer1.Panel2
            // 
            this.splitContainer1.Panel2.Controls.Add(this.btnSave);
            this.splitContainer1.Panel2.Controls.Add(this.btnDeleteEntry);
            this.splitContainer1.Panel2.Controls.Add(this.btnNewEntry);
            this.splitContainer1.Size = new System.Drawing.Size(695, 253);
            this.splitContainer1.SplitterDistance = 215;
            this.splitContainer1.TabIndex = 0;
            // 
            // gridWorktype
            // 
            this.gridWorktype.Dock = System.Windows.Forms.DockStyle.Fill;
            this.gridWorktype.Location = new System.Drawing.Point(0, 0);
            // 
            // gridWorktype
            // 
            this.gridWorktype.MasterTemplate.AllowAddNewRow = false;
            this.gridWorktype.MasterTemplate.AllowDeleteRow = false;
            gridViewTextBoxColumn1.FieldName = "acParentType";
            gridViewTextBoxColumn1.HeaderText = "Starš";
            gridViewTextBoxColumn1.MaxLength = 3;
            gridViewTextBoxColumn1.Name = "acParentType";
            gridViewTextBoxColumn2.FieldName = "acSetOf";
            gridViewTextBoxColumn2.HeaderText = "Vrsta";
            gridViewTextBoxColumn2.MaxLength = 3;
            gridViewTextBoxColumn2.Name = "acSetOf";
            gridViewTextBoxColumn3.FieldName = "acName";
            gridViewTextBoxColumn3.HeaderText = "Naziv";
            gridViewTextBoxColumn3.Name = "acName";
            gridViewCheckBoxColumn1.FieldName = "anIsSubjMandatory";
            gridViewCheckBoxColumn1.HeaderText = "Obv. vn. Subjekta";
            gridViewCheckBoxColumn1.Name = "anIsSubjMandatory";
            gridViewCheckBoxColumn2.FieldName = "anIsChargeable";
            gridViewCheckBoxColumn2.HeaderText = "Zaračunljivo";
            gridViewCheckBoxColumn2.Name = "anIsChargeable";
            gridViewMultiComboBoxColumn1.DisplayMember = "acIdent";
            gridViewMultiComboBoxColumn1.FieldName = "acIdent";
            gridViewMultiComboBoxColumn1.HeaderText = "Artikel";
            gridViewMultiComboBoxColumn1.Name = "acIdent";
            gridViewMultiComboBoxColumn1.ValueMember = "";
            this.gridWorktype.MasterTemplate.Columns.AddRange(new Telerik.WinControls.UI.GridViewDataColumn[] {
            gridViewTextBoxColumn1,
            gridViewTextBoxColumn2,
            gridViewTextBoxColumn3,
            gridViewCheckBoxColumn1,
            gridViewCheckBoxColumn2,
            gridViewMultiComboBoxColumn1});
            this.gridWorktype.Name = "gridWorktype";
            this.gridWorktype.Size = new System.Drawing.Size(695, 215);
            this.gridWorktype.TabIndex = 0;
            this.gridWorktype.CellEditorInitialized += new Telerik.WinControls.UI.GridViewCellEventHandler(this.gridWorktype_CellEditorInitialized);
            this.gridWorktype.RowsChanged += new Telerik.WinControls.UI.GridViewCollectionChangedEventHandler(this.gridWorktype_RowsChanged);
            this.gridWorktype.CellValueChanged += new Telerik.WinControls.UI.GridViewCellEventHandler(this.gridWorktype_CellValueChanged);
            this.gridWorktype.KeyDown += new System.Windows.Forms.KeyEventHandler(this.gridWorktype_KeyDown);
            // 
            // btnSave
            // 
            this.btnSave.Location = new System.Drawing.Point(175, 5);
            this.btnSave.Name = "btnSave";
            this.btnSave.Size = new System.Drawing.Size(75, 23);
            this.btnSave.TabIndex = 2;
            this.btnSave.Text = "Shrani";
            this.btnSave.UseVisualStyleBackColor = true;
            this.btnSave.Click += new System.EventHandler(this.btnSave_Click);
            // 
            // btnDeleteEntry
            // 
            this.btnDeleteEntry.Location = new System.Drawing.Point(94, 5);
            this.btnDeleteEntry.Name = "btnDeleteEntry";
            this.btnDeleteEntry.Size = new System.Drawing.Size(75, 23);
            this.btnDeleteEntry.TabIndex = 1;
            this.btnDeleteEntry.Text = "Briši";
            this.btnDeleteEntry.UseVisualStyleBackColor = true;
            this.btnDeleteEntry.Click += new System.EventHandler(this.btnDeleteEntry_Click);
            // 
            // btnNewEntry
            // 
            this.btnNewEntry.Location = new System.Drawing.Point(13, 5);
            this.btnNewEntry.Name = "btnNewEntry";
            this.btnNewEntry.Size = new System.Drawing.Size(75, 23);
            this.btnNewEntry.TabIndex = 0;
            this.btnNewEntry.Text = "Novo";
            this.btnNewEntry.UseVisualStyleBackColor = true;
            this.btnNewEntry.Click += new System.EventHandler(this.btnNewEntry_Click);
            // 
            // WorkTypeWindow
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.ClientSize = new System.Drawing.Size(695, 253);
            this.Controls.Add(this.splitContainer1);
            this.Icon = ((System.Drawing.Icon)(resources.GetObject("$this.Icon")));
            this.MinimumSize = new System.Drawing.Size(710, 290);
            this.Name = "WorkTypeWindow";
            this.Text = "Vrsta del";
            this.FormClosing += new System.Windows.Forms.FormClosingEventHandler(this.WorkTypeWindow_FormClosing);
            this.Load += new System.EventHandler(this.WorkTypeWindow_Load);
            this.splitContainer1.Panel1.ResumeLayout(false);
            this.splitContainer1.Panel2.ResumeLayout(false);
            ((System.ComponentModel.ISupportInitialize)(this.splitContainer1)).EndInit();
            this.splitContainer1.ResumeLayout(false);
            ((System.ComponentModel.ISupportInitialize)(this.gridWorktype)).EndInit();
            this.ResumeLayout(false);

        }

        #endregion

        private System.Windows.Forms.SplitContainer splitContainer1;
        private System.Windows.Forms.Button btnNewEntry;
        private Telerik.WinControls.UI.RadGridView gridWorktype;
        private System.Windows.Forms.Button btnDeleteEntry;
        private System.Windows.Forms.Button btnSave;


    }
}