﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Windows.Forms;

namespace Softech.Softime
{
    public partial class UserWindow : Form
    {
        public int UserID
        {
            get;
            set;
        }

        public Datalayer Datalayer
        {
            get;
            set;
        }

        public bool AllowChangeUser
        {
            get;
            set;
        }

        private List<UserEntry> users;

        public UserWindow()
        {
            InitializeComponent();
        }

        private void UserWindow_Load(object sender, EventArgs e)
        {
            users = Datalayer.GetUsers();
            foreach (var user in users)
            {
                cbUsers.Items.Add(user);
            }
            cbUsers.SelectedItem = users.Where(o => o.Id == UserID).Single();
            if (!AllowChangeUser)
                cbUsers.Enabled = false;
        }

        private void cbUsers_SelectedIndexChanged(object sender, EventArgs e)
        {
            var user = (UserEntry)cbUsers.SelectedItem;
            if (user.Id == UserID)
                return;

            UserID = user.Id;
        }

        private void btnSave_Click(object sender, EventArgs e)
        {
            if (tbOldPassword.Text.Length > 0)
            {
                var oldpassword = tbOldPassword.Text;
                var newpassword = tbNewPassword.Text;
                var confirmed   = tbConfirmPassword.Text;
                var username    = (UserEntry)cbUsers.SelectedItem;

                if (newpassword == confirmed)
                {
                    bool result = Datalayer.ChangePassword(oldpassword, newpassword, UserID, username.WebUsername);
                    if (result)
                        MessageBox.Show("Geslo je bilo uspešno spremenjeno", "Sprememba gesla", MessageBoxButtons.OK, MessageBoxIcon.Information);
                    else
                        MessageBox.Show("Gesla ni bilo možno spremenit", "Sprememba gesla", MessageBoxButtons.OK, MessageBoxIcon.Error);
                }
                else
                    MessageBox.Show("Gesli se ne ujemata", "Neujemanje gesel", MessageBoxButtons.OK, MessageBoxIcon.Error);
            }
            else
                MessageBox.Show("Prosim izpolnite vsa polja", "Napaka", MessageBoxButtons.OK, MessageBoxIcon.Exclamation);
        }
    }
}
