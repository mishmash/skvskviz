SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
IF NOT EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[dbo].[_Sof_spSofTimeLogOn]') AND type in (N'P', N'PC'))
BEGIN
EXEC dbo.sp_executesql @statement = N'CREATE PROCEDURE [dbo].[_Sof_spSofTimeLogOn]
@cUserID VARCHAR(30)='''',
@acPass varchar(50)='''',
@acNewPass varchar(50)='''',
@acHostIP VARCHAR(16)='''',
@acType CHAR(1)=''P'',
@abVerified BIT OUTPUT,
@anUserId INT OUTPUT,
@acUserStatus CHAR(1) OUTPUT
AS 
SET NOCOUNT ON;

SET @acUserStatus = ''U'';

/* 13.02.2013
 @acType
 ''O'' for logout
 ''P'' for Login
 ''C'' for PassWord Change''
*/

DECLARE @Alowed CHAR(1), @Hostname VARCHAR(255), @Database VARCHAR(255), @anActionType INT,
		@NetMac VARCHAR(14)

IF @acType=''I'' 
SET @anActionType=7
IF @acType=''O'' 
SET @anActionType=8

SET @abVerified = 0;

SELECT @NetMac=net_Address FROM master..sysprocesses
	WHERE hostname=HOST_NAME()
	AND spid=@@SPID
	
SELECT @anUserId=ISNULL((SELECT anUserId FROM [dbo].[tHE_SetSubjContact] WHERE acWebUserID=@cUserID and acPassword=ltrim(rtrim(@acPass))),0)

SET @Hostname=HOST_NAME()
SET @Database=DB_NAME()

IF @anUserId <> 0
 SET @abVerified = 1; 
 
 IF @cUserID =''miha''
	 SET @acUserStatus = ''A'';

IF @acType = ''C'' AND @abVerified = 1
BEGIN

IF @acNewPass =''''
	SET @abVerified = 0;

IF @acNewPass <> ''''
UPDATE [dbo].[tHE_SetSubjContact] set  acPassword=ltrim(rtrim(@acNewPass)), adTimeChg=GETDATE() 
	where anUserID= @anUserId and acPassword=ltrim(rtrim(@acPass));
	
END

/* DalaLab Log insert */
INSERT INTO tpa_SysLog (acAppL, anUserID, adTime, anHelpContext, acType, acHostName, acDatabaseName, acValue)
VALUES (''ST'', @anUserID, GETDATE(), 0, @acType, @Hostname, @DataBase, 
	''SofTime '' + 
	case when @acType = ''P'' Then ''prijava'' 
		when @acType =''O'' then ''odjava''
		when @acType=''C'' then ''sprememba gesla''
		else '''' end +'', user='' + @cUserID + '' ,Net ID: ''+@NetMac + '', '' + @acHostIP )

--select * from tpa_SysLog order by a

' 
END
GO
/****** Object:  StoredProcedure [dbo].[_Sof_spSofTimeGetEntities]    Script Date: 03/04/2013 13:22:14 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
IF NOT EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[dbo].[_Sof_spSofTimeGetEntities]') AND type in (N'P', N'PC'))
BEGIN
EXEC dbo.sp_executesql @statement = N'CREATE PROCEDURE [dbo].[_Sof_spSofTimeGetEntities]
@act  CHAR(1)=''S'',
@phrase varchar(100)=''''
AS

IF @phrase IS NULL
	SET @phrase = '''';

SET @phrase = ''%'' + @phrase  +  ''%''

IF @act = ''S'' --Subjekti
BEGIN
select s.acSubject, s.acName2, s.acPost, p.acName as acPostname, s.acCode  
	from dbo.tHE_SetSubj s
	left join dbo.tHE_SetPostCode p on (s.acPost=p.acPost)
	where 1=1
	AND s.acSubject <> ''''
	AND (s.acBuyer=''T'' OR acSupplier=''T'') 
	AND (s.acSubject like @phrase  OR s.acName2 like @phrase)
	option(fast 100) 
END;

IF @act = ''U'' --Uporabniki
BEGIN
Select acWebUserID, anUserID, ltrim(rtrim(isnull(acName,''''))) + '' '' + ltrim(rtrim(isnull(acSurname,''''))) as acUser from dbo.tHE_SetSubjContact where acWebUserID <> ''''
END;

IF @act = ''I'' -- IDENTI
BEGIN
select acIdent, acName, acClassif, acClassif2, acDept, acCostDrv, acSetOfItem from dbo.tHE_SetItem 
	where 1=1
	and acActive=''T''
	and acIdent <> ''''
	AND (acIdent like @phrase  OR acName like @phrase)
	option(fast 100)
END;

IF @act = ''D'' -- Šifrant Vrst Del 
BEGIN
select acSetOf, acName 
	,anUsagePrc --Working hours usage for this calendar entry type
	,anColorBack --Background color in calendar
	,anColorText --Text color in calendar
	,acParentType --Parent entry type for creating hierarchical structure of entry types
	,acIdent --	ID of item with which this activity is posted
	,anIsSubjMandatory --	Is subject required?
	,anIsChargeable --Zaračunljivo
	,anIsMeeting
	,anIsService
	,acOperation -- + ali Minus - Ure se seštevajo ali odštevajo
  from dbo.tPA_SetCalendarEntryType  where acType = ''C'' AND ACSETOF <> ''''
END

IF @act = ''O'' -- Oddelek
BEGIN
select acSubject, acName2 from dbo.tHE_SetSubj where 1=1
	AND acActive=''T''
	AND acDept =''T'' 
	and acSubject <> '''' order by acSubject;
END

IF @act = ''C'' -- CostDriver
BEGIN
select c.acCostDrv, c.acName, c.acStatus, s.acActive from dbo.tHE_CostDrv c
	LEFT JOIN tHE_CostDrvStatus s on (s.acStatus=c.acStatus)
END
' 
END
GO
/****** Object:  StoredProcedure [dbo].[_Sof_spSofTimeGetData]    Script Date: 03/04/2013 13:22:14 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
IF NOT EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[dbo].[_Sof_spSofTimeGetData]') AND type in (N'P', N'PC'))
BEGIN
EXEC dbo.sp_executesql @statement = N'CREATE procedure [dbo].[_Sof_spSofTimeGetData]
@acDelcavec varchar(50)= '''',
@acSubject varchar(30) = '''',
@adDateFrom datetime = NULL,
@adDateTo datetime = NULL,
@abUpost bit = 0,
@abAll bit = 0,
@acTableName varchar(255) = NULL,
@acSofTimeExclude varchar(255)= NULL,
@acSofTimeTypeExclude varchar(255)= NULL
AS 
SET Nocount ON;

DECLARE @SqlX varchar(max), @acDelavecID varchar(1);

set @acDelavecID=(select convert(varchar, isnull(anuserid,0)) from the_setsubjcontact 
	where (ltrim(rtrim(acWebUserID))+ '' - ''+ltrim(rtrim(acName)) + '' '' + ltrim(rtrim(acSurname)) )= @acDelcavec)

SET @SqlX ='' INSERT INTO '' +@acTableName + ''
SELECT P.anUserId, P.adBegin, P.acSetOf,
P.acSubject, datediff(ss, ''''1899-12-30 00:00:00.000'''', p.adTime) as cas, case when p.acTime <> '''''''' 
then ''''Opravilo št: '''' + p.acTime + '''' - ''''+ convert(varchar(max), p.acNote) 
else p.acNote end as acNote, D.acWorker, V.acName AS VNaziv,
P.acDept, P.acCostDrv, p.anid
FROM 
dbo.vPA_SysUsersSelect D, 
dbo.tHE_Chronos P
left join dbo.tPA_SetCalendarEntryType V on (P.acSetOf = V.acSetOf)
WHERE 1=1
AND P.anUserId = D.anUserId''

IF (@adDateFrom IS NOT NULL AND @adDateFrom > ''1900-01-01'')
SET @SqlX = @SqlX + '' AND CAST(P.adBegin as DATE) >= '''''' + Convert(varchar, @adDateFrom ,111)+'''''' ''

IF (@adDateTO IS NOT NULL AND @adDateTo > ''1900-01-01'')
SET @SqlX = @SqlX + '' AND cast(P.adBegin as DATE) <= '''''' + convert(varchar, @adDateTO ,111) + '''''' ''

IF @acSubject <> ''''
SET @SqlX = @SqlX + '' AND p.acSubject like '''''' + @acSubject +''%'' + '''''' '' 

SET @SqlX = @SqlX + '' AND p.acSubject not in (select naziv from ''+ @acSofTimeExclude + '')''

IF @acDelavecID <> 0
SET @SqlX = @SqlX + '' AND p.anUserId = '' + @acDelavecID

IF @abAll <> 1 AND @abUpost = 0
SET @SqlX = @SqlX + '' AND  v.acSetOf not in (select tip from '' + @acSofTimeTypeExclude + '')'' 

IF @abAll <> 1 AND @abUpost = 1
SET @SqlX = @SqlX + '' AND  v.acSetOf IN (select tip from '' + @acSofTimeTypeExclude + '')'' 


SET @SqlX = @SqlX + '' order by p.acSubject, p.acCostDrv, p.adBegin''

exec(@SqlX)
' 
END
GO
/****** Object:  StoredProcedure [dbo].[_Sof_spSofTimeManageEntities]    Script Date: 03/04/2013 13:22:14 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
IF NOT EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[dbo].[_Sof_spSofTimeManageEntities]') AND type in (N'P', N'PC'))
BEGIN
EXEC dbo.sp_executesql @statement = N'CREATE PROCEDURE [dbo].[_Sof_spSofTimeManageEntities]
@act char(1) = '''',
@anUserID INT,
@acSetOf char(3) = '''',
@acParentType Varchar(3) = '''',
@acName Varchar(40) = '''',
@anUsagePrc INT = 0,
@anColorBack INT = 0,
@anColorText INT = 0,
@acIdent CHAR(16) ='''',
@anIsSubjMandatory BIT = 0,
@anIsChargeable BIT = 0,
@anIsMeeting BIT = 0,
@anIsService BIT = 0, 
@acOperation CHAR(1) = ''''
as 

IF @act = ''I''
begin
INSERT INTO dbo.tPA_SetCalendarEntryType ([acSetOf]
      ,[acName]
      ,[anUsagePrc]
      ,[anColorBack]
      ,[anColorText]
      ,[anUserIns]
      ,[adTimeIns]
      ,[anUserChg]
      ,[adTimeChg]
      ,[acType]
      ,[acParentType]
      ,[acIdent]
      ,[anIsSubjMandatory]
      ,[anIsShowInChronos]
      ,[anIsChargeable]
      ,[anIsMeeting]
      ,[anIsService]
      ,[acOperation])
VALUES (@acSetOf, @acName, @anUsagePrc, @anColorBack, @anColorText,
	@anUserID, GETDATE(), @anUserID, GETDATE(), ''C'', @acParentType, @acIdent, @anIsSubjMandatory,1,
	 @anIsChargeable, @anIsMeeting, @anIsService, @acOperation);
END

IF @act = ''U''
begin

Update dbo.tPA_SetCalendarEntryType set 
      [acName] = @acName
      ,[anUsagePrc] =@anUsagePrc
      ,[anColorBack]=@anColorBack
      ,[anColorText]=@anColorText
      ,[anUserChg]=@anUserID
      ,[adTimeChg]=GETDATE()
      ,[acParentType]=@acParentType
      ,[acIdent]=@acIdent
      ,[anIsSubjMandatory]=@anIsSubjMandatory
      ,[anIsChargeable]=@anIsChargeable
      ,[anIsMeeting]=@anIsMeeting
      ,[anIsService]=@anIsService
      ,[acOperation]=@acOperation
		WHere [acSetOf] = @acSetOf;
END

IF @act = ''D''
begin
	DELETE FROM dbo.tPA_SetCalendarEntryType WHere [acSetOf] = @acSetOf
	AND [acSetOf] not in (select [acSetOf] from tHE_Chronos WHERE acSetOf=@acSetOf);
END

' 
END
GO
/****** Object:  StoredProcedure [dbo].[_Sof_spSofTimeManageDetail]    Script Date: 03/04/2013 13:22:14 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
IF NOT EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[dbo].[_Sof_spSofTimeManageDetail]') AND type in (N'P', N'PC'))
BEGIN
EXEC dbo.sp_executesql @statement = N'CREATE PROCEDURE [dbo].[_Sof_spSofTimeManageDetail]
@act CHAR(1)=''L'', /* L - List, I-Insert, U-Update */
@adTimeFrom datetime = NULL,
@adTimeTo datetime = NULL,
@anUserID INT = 0,
@anID INT = NULL,
@adBegin Datetime = NULL,
@acSubject varchar(30) = '''',
@acSetOf Varchar(3)='''',
@acDescr Varchar(255)='''',
@anTime INT = NULL, 
@acTime varchar(50) = '''', 
@acDept varchar(30) = '''',
@acCostDrv varchar(16)='''',
@acSolved CHAR(1)=''F'',
@adDateOfSolution datetime = NULL,
@anCallId INT = NULL,
@acContactPrsn Varchar(30) = '''',
@anWith_Cont INT = NULL,
@acWho_Sub Varchar(30) = '''',
@anWho_Cont INT = NULL,
@acNote VARCHAR(MAX)=NULL,
@adNextCall datetime = NULL
AS

Declare @sqlX  Varchar(Max), @adTime datetime;

IF @act = ''L''
BEGIN

SET @sqlX=''Select c.anId, c.anUserId, c.adBegin, c.acSubject, 
	c.acSetOf, c.acDescr, DATEDIFF(ss,''''1899-12-30'''', c.adTIme ) as anTime, c.acTime
	,c.acDept, c.acCostDrv, c.acSolved, c.adDateOfSolution, anCallId
	, c.acContactPrsn, c.anWith_Cont, c.acWho_Sub, c.anWho_Cont, c.acNote, c.adNextCall
	,c.adTimeChg, c.anUserChg
	,ltrim(rtrim(isnull(u.acName,''''''''))) + '''' '''' + ltrim(rtrim(isnull(u.acSurname,''''''''))) as acUser
	
	 from dbo.tHE_Chronos c 
	left join dbo.tHE_SetSubjContact u ON ( u.anUserId = c.anUserId)
	where 1=1''
IF @anUserID <> -1 
	SET @sqlX= @sqlX + '' AND c.anUserId='' + convert(varchar, @anUserID) 
	
IF @adTimeFrom IS NOT NULL
 SET @sqlX= @sqlX + '' AND convert(varchar, c.adBegin, 111) > ='' +CHAR(39) +  convert(varchar, @adTimeFrom, 111) + CHAR(39)
	
IF @adTimeTo IS NOT NULL
 SET @sqlX= @sqlX + '' AND convert(varchar, c.adBegin, 111)  <= '' +CHAR(39) +  convert(varchar, @adTimeTo, 111) + CHAR(39)
EXEC (@sqlX);
END

IF @act = ''I''
BEGIN

SET @anId = ISNULL((select MAX(anId) + 1 from tHE_Chronos),1);
SET @adTime=DATEADD(ss, @anTime, ''1899-12-30'');
 
INSERT INTO dbo.tHE_Chronos ( anId, [anUserId] ,[adBegin] ,[acSubject] 
      ,[acSetOf] ,[acDescr],[adTime] ,[acTime],[acDept]
      ,[acCostDrv],[acSolved] ,[adDateOfSolution] ,[anCallId]
      ,[acContactPrsn],[anWith_Cont] ,[acWho_Sub],[anWho_Cont]
      ,[anUserIns],[adTimeIns] ,[anUserChg] ,[adTimeChg] ,[acNote]
      ,[adNextCall], acType)
VALUES (@anId, @anUserID, @adBegin, @acSubject,
	@acSetOf, @acDescr, @adTime, @acTime, @acDept, 
	@acCostDrv, @acSolved, @adDateOfSolution, @anCallId,
	@acContactPrsn, @anWith_Cont, @acWho_Sub, @anWho_Cont,
	@anUserID, GETDATE(), @anUserID, GETDATE(), @acNote, @adNextCall, ''C'')
	 
Select @anId as anID; 
END

IF @act = ''U''
BEGIN
SET @adTime=DATEADD(ss, @anTime, ''1899-12-30'');

 UPDATE dbo.tHE_Chronos 
 SET  [anUserId] = @anUserID
	,[adBegin] = @adBegin
	,[acSubject] = @acSubject 
    ,[acSetOf] = @acSetOf
    ,[acDescr] = @acDescr
    ,[adTime] = @adTime
    ,[acTime] = @acTime
    ,[acDept] = @acDept
    ,[acCostDrv] = @acCostDrv
    ,[acSolved] = @acSolved 
    ,[adDateOfSolution] = @adDateOfSolution
    ,[anCallId] = @anCallId
    ,[acContactPrsn] = @acContactPrsn
    ,[anWith_Cont] = @anWith_Cont,
     [acWho_Sub] = @acWho_Sub
     ,[anWho_Cont] = @anWho_Cont
      ,[anUserChg] = @anUserID 
      ,[adTimeChg] = GETDATE()
      ,[acNote] = @acNote
      ,[adNextCall] = @adNextCall
      WHere anId = @anID;
	 
END

IF @act = ''D''
BEGIN
DELETE FROM dbo.tHE_Chronos where anId = @anID;
END' 
END
GO
