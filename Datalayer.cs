﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Data.SqlClient;
using System.Data;
using System.Windows.Forms;
using System.Text.RegularExpressions;
using System.Reflection;
using System.IO;

namespace Softech.Softime
{
    public class Datalayer
    {
        public string ConnectionString
        {
            get;
            set;
        }

        public Datalayer(string connectionString)
        {
            ConnectionString = connectionString;
        }

        public bool TestConnection()
        {
            SqlConnection connection = new SqlConnection(ConnectionString);
            try
            {
                connection.Open();
                connection.Close();
            }
            catch (Exception)
            {
                return false;
            }
            return true;
        }

        /// <summary>
        /// </summary>
        /// <param name="sql"></param>
        /// <returns></returns>
        public bool ExecuteSql(string sql)
        {
            bool error = false;

            IEnumerable<string> commands = Regex.Split(sql, @"^\s*GO\s*$", RegexOptions.Multiline | RegexOptions.IgnoreCase);
            using (SqlConnection connection = new SqlConnection(ConnectionString))
            {
                connection.Open();
                foreach (string command in commands)
                {
                    if (command.Trim() != string.Empty)
                        error = (new SqlCommand(command, connection).ExecuteNonQuery()) == 0;
                }
            }
            return error;
        }


        public bool StoredProceduresInstalled()
        {
            using (SqlConnection connection = new SqlConnection(ConnectionString))
            using (SqlCommand command = new SqlCommand())
            {
                command.Connection = connection;
                connection.Open();

                using (Stream stream = Assembly.GetCallingAssembly().GetManifestResourceStream("Softech.Softime.Resources.SPExists.sql"))
                using (StreamReader reader = new StreamReader(stream))
                {
                    command.CommandText = reader.ReadToEnd();
                }

                SqlDataAdapter adapter = new SqlDataAdapter(command);
                DataSet dataset = new DataSet();
                adapter.Fill(dataset);

                int exists = int.Parse(dataset.Tables[0].Rows[0]["Result"].ToString());
                return exists != 0;
            }
        }

        public bool ConfirmKey(string key)
        {
            SqlDataAdapter adapter = new SqlDataAdapter();
            DataSet ds = new DataSet();

            using (SqlConnection connection = new SqlConnection(ConnectionString))
            {
                connection.Open();

                using (SqlCommand command = connection.CreateCommand())
                {
                    command.CommandText = "SELECT TOP 1 acSerialNo FROM dbo.tpa_SysparamSys WHERE acSerialNo = @Serial";
                    command.Parameters.AddWithValue("@Serial", key);
                    adapter.SelectCommand = command;

                    adapter.Fill(ds);
                    if (ds.Tables[0].Rows.Count > 0)
                        return true;
                    else
                        return false;
                }
            }

        }

        public List<DatabaseEntry> GetDatabases()
        {
            DataSet dataset = new DataSet();
            List<DatabaseEntry> entries = new List<DatabaseEntry>();
            try
            {
                using (SqlConnection connection = new SqlConnection(ConnectionString))
                using (SqlCommand command = CreateStoredProcedureCommand("pPA_GetDatabases", connection))
                {
                    connection.Open();

                    command.Parameters.AddWithValue("@p_Refresh", "F");

                    SqlDataAdapter adapter = new SqlDataAdapter(command);
                    adapter.Fill(dataset);

                    foreach (DataRow row in dataset.Tables[0].Rows)
                    {
                        entries.Add(new DatabaseEntry(row["DbName"].ToString(), row["Podjetje"].ToString()));
                    }
                }
            }
            catch (Exception)
            {
                throw;
            }
            return entries;
        }

        public List<UserEntry> GetUsers ()
        {
            DataSet dataset = new DataSet();
            List<UserEntry> entries = new List<UserEntry>();
            try
            {
                using (SqlConnection connection = new SqlConnection(ConnectionString))
                using (SqlCommand command = CreateStoredProcedureCommand("_Sof_spSofTimeGetEntities", connection))
                {
                    connection.Open();

                    command.Parameters.AddWithValue("@act", "U");

                    SqlDataAdapter adapter = new SqlDataAdapter(command);
                    adapter.Fill(dataset);

                    foreach (DataRow row in dataset.Tables[0].Rows)
                    {
                        entries.Add(new UserEntry(row["acUser"].ToString(), row["acWebUserID"].ToString(), int.Parse(row["anUserID"].ToString())));
                    }
                }
            }
            catch (Exception)
            {
                throw;
            }
            return entries;
        }

        public string GetCurrentDatabase()
        {
            SqlConnectionStringBuilder builder = new SqlConnectionStringBuilder(ConnectionString);
            return builder.InitialCatalog;
        }

        public void SetCurrentDatabase(string name)
        {
            SqlConnectionStringBuilder builder = new SqlConnectionStringBuilder(ConnectionString);
            builder.InitialCatalog = name;
            ConnectionString = builder.ConnectionString;
        }

        public bool Login(string username, string password, out int userid, out UserType userType)
        {
            using (SqlConnection connection = new SqlConnection(ConnectionString))
            using (SqlCommand command = CreateStoredProcedureCommand("_Sof_spSofTimeLogOn", connection))
            {
                connection.Open();
                command.Parameters.AddWithValue("@cUserID",  username);
                command.Parameters.AddWithValue("@acPass",   password);
                command.Parameters.AddWithValue("@acHostIP", Extensions.GetIPAddress());
                command.Parameters.AddWithValue("@acType",   "P");

                SqlParameter verified = new SqlParameter("@abVerified", -1);
                SqlParameter userId   = new SqlParameter("@anUserId",   -1);
                SqlParameter type     = new SqlParameter("@acUserStatus", -1);

                verified.SqlDbType = SqlDbType.Bit;
                verified.Direction = ParameterDirection.Output;
                userId.SqlDbType   = SqlDbType.Int;
                userId.Direction   = ParameterDirection.Output;
                type.SqlDbType     = SqlDbType.Char;
                type.Direction     = ParameterDirection.Output;

                command.Parameters.Add(verified);
                command.Parameters.Add(userId);
                command.Parameters.Add(type);

                command.ExecuteNonQuery();

                bool loggedIn  = bool.Parse(command.Parameters["@abVerified"].Value.ToString());
                userid         = int.Parse(command.Parameters["@anUserId"].Value.ToString());
                char privilege = char.Parse(command.Parameters["@acUserStatus"].Value.ToString());

                switch (privilege)
                {
                    case 'U':
                        userType = UserType.User;
                        break;

                    case 'A':
                        userType = UserType.Admin;
                        break;

                    default:
                        userType = UserType.Unknown;
                        break;
                }

                return loggedIn;
            }
        }

        public bool ChangePassword(string password, string newpassword, int user, string username)
        {
            using (SqlConnection connection = new SqlConnection(ConnectionString))
            using (SqlCommand command = CreateStoredProcedureCommand("_Sof_spSofTimeLogOn", connection))
            {
                connection.Open();
                command.Parameters.AddWithValue("@cUserID", username);
                command.Parameters.AddWithValue("@acPass",    password);
                command.Parameters.AddWithValue("@acNewPass", newpassword);
                command.Parameters.AddWithValue("@acType",    "C");

                #region Output parametri
                SqlParameter verified = new SqlParameter("@abVerified", -1);
                SqlParameter userId = new SqlParameter("@anUserId", -1);
                SqlParameter type = new SqlParameter("@acUserStatus", -1);

                verified.SqlDbType = SqlDbType.Bit;
                verified.Direction = ParameterDirection.Output;
                userId.SqlDbType = SqlDbType.Int;
                userId.Direction = ParameterDirection.Output;
                type.SqlDbType = SqlDbType.Char;
                type.Direction = ParameterDirection.Output;

                command.Parameters.Add(verified);
                command.Parameters.Add(userId);
                command.Parameters.Add(type);
                #endregion

                command.ExecuteNonQuery();

                bool changed = bool.Parse(command.Parameters["@abVerified"].Value.ToString());
                return changed;
            }
        }

        public DataTable GetStroskovniNosilci(string phrase = "")
        {
            DataSet dataset = new DataSet();
            List<WorkTypeEntry> entries = new List<WorkTypeEntry>();
            try
            {
                using (SqlConnection connection = new SqlConnection(ConnectionString))
                using (SqlCommand command = CreateStoredProcedureCommand("_Sof_spSofTimeGetEntities", connection))
                {
                    connection.Open();

                    command.Parameters.AddWithValue("@act", "C");
                    command.Parameters.AddWithValue("@phrase", phrase);
                    SqlDataAdapter adapter = new SqlDataAdapter(command);
                    adapter.Fill(dataset);

                    var table = dataset.Tables[0];
                    table.Columns.Remove("acStatus");
                    return dataset.Tables[0];
                }
            }
            catch (Exception)
            {
                throw;
            }
        }

        public List<WorkTypeEntry> GetWorkTypes(bool flat = false)
        {
            DataSet dataset = new DataSet();
            List<WorkTypeEntry> entries = new List<WorkTypeEntry>();
            try
            {
                using (SqlConnection connection = new SqlConnection(ConnectionString))
                using (SqlCommand command = CreateStoredProcedureCommand("_Sof_spSofTimeGetEntities", connection))
                {
                    connection.Open();

                    command.Parameters.AddWithValue("@act", "D");
                    SqlDataAdapter adapter = new SqlDataAdapter(command);
                    adapter.Fill(dataset);

                    // Spremeni to v rekurzivno funkcijo
                    foreach (DataRow row in dataset.Tables[0].Rows)
                    {
                        WorkTypeEntry parent = new WorkTypeEntry();
                        parent.Name          = row["acName"].ToString().Trim();
                        parent.Identifier    = row["acSetOf"].ToString().Trim();
                        parent.pIdentifier   = row["acParentType"].ToString().Trim();
                        parent.Mandatory     = bool.Parse(row["anIsSubjMandatory"].ToString());
                        parent.Chargeable    = bool.Parse(row["anIsChargeable"].ToString());

                        if (flat)
                        {
                            entries.Add(parent);
                            continue;
                        }
                        foreach (DataRow c in dataset.Tables[0].Rows)
                        {
                            WorkTypeEntry child = new WorkTypeEntry();
                            child.Name          = c["acName"].ToString().Trim();
                            child.Identifier    = c["acSetOf"].ToString().Trim();
                            child.pIdentifier   = c["acParentType"].ToString().Trim();
                            child.Mandatory     = bool.Parse(c["anIsSubjMandatory"].ToString());
                            child.Chargeable    = bool.Parse(c["anIsChargeable"].ToString());

                            if (parent.Identifier == child.pIdentifier)
                                parent.Children.Add(child);
                        }
                        if (parent.pIdentifier == string.Empty)
                            entries.Add(parent);
                    }
                }
            }
            catch (Exception)
            {
                throw;
            }

            return entries;
        }

        public DataTable GetWorkTypesDt(bool flat = false)
        {
            DataSet dataset = new DataSet();
            List<WorkTypeEntry> entries = new List<WorkTypeEntry>();
            try
            {
                using (SqlConnection connection = new SqlConnection(ConnectionString))
                using (SqlCommand command = CreateStoredProcedureCommand("_Sof_spSofTimeGetEntities", connection))
                {
                    connection.Open();

                    command.Parameters.AddWithValue("@act", "D");
                    SqlDataAdapter adapter = new SqlDataAdapter(command);
                    adapter.Fill(dataset);

                    var table = dataset.Tables[0];
                    table.Columns.Remove("anUsagePrc");
                    table.Columns.Remove("anColorBack");
                    table.Columns.Remove("anColorText");
                    table.Columns.Remove("acParentType");
                    table.Columns.Remove("anIsSubjMandatory");
                    table.Columns.Remove("anIsChargeable");
                    table.Columns.Remove("anIsMeeting");
                    table.Columns.Remove("anIsService");
                    table.Columns.Remove("acOperation");

                    table.Columns.Add("acRealName");

                    foreach (DataRow row in table.Rows)
                    {
                        row["acSetOf"] = row["acSetOf"].ToString().Trim();
                        row["acName"]  = row["acName"].ToString().Trim();

                        var ID   = row["acSetOf"].ToString();
                        var name = row["acName"].ToString();
                        row["acRealName"] = string.Format("{0} - {1}", ID, name);
                    }

                    return dataset.Tables[0];
                }
            }
            catch (Exception)
            {
                throw;
            }
        }

        public DataTable GetArticles(string hint = "")
        {
            using (SqlConnection connection = new SqlConnection(ConnectionString))
            using (SqlCommand command = CreateStoredProcedureCommand("_Sof_spSofTimeGetEntities", connection))
            {
                connection.Open();

                command.Parameters.AddWithValue("@act", "I");
                command.Parameters.AddWithValue("@phrase", hint);

                DataSet dataset = new DataSet();
                SqlDataAdapter adapter = new SqlDataAdapter(command);
                adapter.Fill(dataset);

                var table = dataset.Tables[0];
                table.Columns.Remove("acClassIf");
                table.Columns.Remove("acClassIf2");
                table.Columns.Remove("acDept");
                table.Columns.Remove("acCostDrv");
                table.Columns.Remove("acSetOfItem");

                return table;
            }
        }

        private bool DoworkWorktype(char action, WorkTypeEntry entry, int user)
        {
            try
            {
                using (SqlConnection connection = new SqlConnection(ConnectionString))
                using (SqlCommand command = CreateStoredProcedureCommand("_Sof_spSofTimeManageEntities", connection))
                {
                    connection.Open();

                    command.Parameters.AddWithValue("@act", action);
                    command.Parameters.AddWithValue("@anUserID", user);
                    command.Parameters.AddWithValue("@acSetOf", entry.Identifier);
                    command.Parameters.AddWithValue("@acParentType", entry.pIdentifier);
                    command.Parameters.AddWithValue("@acName", entry.Name);
                    command.Parameters.AddWithValue("@anIsSubjMandatory", entry.Mandatory);
                    command.Parameters.AddWithValue("@acIdent", entry.Article);
                    command.Parameters.AddWithValue("@anIsChargeable", entry.Chargeable);

                    command.Parameters.AddWithValue("@anUsagePrc", 0);
                    command.Parameters.AddWithValue("@anColorBack", 0);
                    command.Parameters.AddWithValue("@anColorText", 0);
                    command.Parameters.AddWithValue("@anIsMeeting", 0);
                    command.Parameters.AddWithValue("@anIsService", 0);
                    command.Parameters.AddWithValue("@acOperation", "");

                    return command.ExecuteNonQuery() > 0;
                }
            }
            catch (Exception)
            {
                throw;
            }
        }

        public bool InsertWorktype(WorkTypeEntry entry, int user)
        {
            return DoworkWorktype('I', entry, user);
        }

        public bool UpdateWorktype(WorkTypeEntry entry, int user)
        {
            return DoworkWorktype('U', entry, user);
        }

        public DataTable GetSubjekti(string hint = "")
        {
            DataSet dataset = new DataSet();
            try
            {
                using (SqlConnection connection = new SqlConnection(ConnectionString))
                using (SqlCommand command = CreateStoredProcedureCommand("_Sof_spSofTimeGetEntities", connection))
                {
                    connection.Open();

                    command.Parameters.AddWithValue("@act", "S");
                    command.Parameters.AddWithValue("@phrase", hint);

                    SqlDataAdapter adapter = new SqlDataAdapter(command);
                    adapter.Fill(dataset);

                    dataset.Tables[0].Columns.Remove("acPost");
                    dataset.Tables[0].Columns.Remove("acPostName");
                    dataset.Tables[0].Columns.Remove("acCode");

                    foreach (DataRow row in dataset.Tables[0].Rows)
                    {
                        row["acSubject"] = row["acSubject"].ToString().Trim();
                        row["acName2"]   = row["acName2"].ToString().Trim();
                    }

                    return dataset.Tables[0];
                }
            }
            catch (Exception)
            {
                throw;
            }
        }

        public DataTable GetOpravki(DateTime from, DateTime to, int user = -1)
        {
            DataSet dataset = new DataSet();
            using (SqlConnection connection = new SqlConnection(ConnectionString))
            using (SqlCommand command = CreateStoredProcedureCommand("_Sof_spSofTimeManageDetail", connection))
            {
                connection.Open();

                command.Parameters.AddWithValue("@act", "L");
                command.Parameters.AddWithValue("@anUserID", user);
                command.Parameters.AddWithValue("@adTimeTo", to);
                command.Parameters.AddWithValue("@adTimeFrom", from);

                SqlDataAdapter adapter = new SqlDataAdapter(command);
                adapter.Fill(dataset);

                var table = dataset.Tables[0];
                var dtCloned = table.Clone();

                table.Columns.Add("anDateTime", typeof(DateTime));
                table.Columns.Add("acIsSolved", typeof(bool));
                foreach (DataRow row in table.Rows)
                {
                    TimeSpan span = TimeSpan.FromSeconds(int.Parse(row["anTime"].ToString()));
                    row["anDateTime"] = new DateTime(span.Ticks);
                    row["acIsSolved"] = row["acSolved"].ToString().Equals("T") ? true : false;
                    dtCloned.ImportRow(row);
                }
                return table;
            }
        }

        public bool InsertOpravek(Opravek entry, out int recordId)
        {
            return DoOpravki('I', entry, out recordId);
        }

        public bool UpdateOpravek(Opravek entry)
        {
            int discard = -1;
            return DoOpravki('U', entry, out discard);
        }

        private bool DoOpravki(char action, Opravek entry, out int recordId)
        {
            recordId = -1;
            using (SqlConnection connection = new SqlConnection(ConnectionString))
            using (SqlCommand command = CreateStoredProcedureCommand("_Sof_spSofTimeManageDetail", connection))
            {
                connection.Open();

                command.Parameters.AddWithValue("@act", action);
                if (action == 'U')
                {
                    command.Parameters.AddWithValue("@anID", entry.ID);
                }

                command.Parameters.AddWithValue("@anUserID", entry.UserID);
                command.Parameters.AddWithValue("@adBegin", entry.Zacetek);
                command.Parameters.AddWithValue("@acSubject", entry.Subjekt);
                command.Parameters.AddWithValue("@anTime", entry.Time);
                command.Parameters.AddWithValue("@acSolved", entry.SolvedString);
                command.Parameters.AddWithValue("@adDateOfSolution", entry.DateSolved);
                command.Parameters.AddWithValue("@acSetOf", entry.Vrsta);
                command.Parameters.AddWithValue("@acDept", entry.Department);
                command.Parameters.AddWithValue("@acDescr", entry.Opis);
                command.Parameters.AddWithValue("@acNote", entry.Komentar);
                command.Parameters.AddWithValue("@acCostDrv", entry.StroskovniNosilec);

                if (action == 'I')
                {
                    SqlDataAdapter adapter = new SqlDataAdapter(command);
                    DataSet dataset = new DataSet();

                    adapter.Fill(dataset);
                    if (dataset.Tables != null)
                    {
                        recordId = int.Parse(dataset.Tables[0].Rows[0]["anID"].ToString());
                        return true;
                    }
                    return false;
                }
                return command.ExecuteNonQuery() > 0;
            }
        }

        public bool DeleteOpravek(int id)
        {
            using (SqlConnection connection = new SqlConnection(ConnectionString))
            using (SqlCommand command = CreateStoredProcedureCommand("_Sof_spSofTimeManageDetail", connection))
            {
                connection.Open();

                command.Parameters.AddWithValue("@act", 'D');
                command.Parameters.AddWithValue("@anId", id);

                return command.ExecuteNonQuery() > 0;
            }
        }

        public DataTable GetOddeleki(string hint = "")
        {
            DataSet dataset = new DataSet();
            try
            {
                using (SqlConnection connection = new SqlConnection(ConnectionString))
                using (SqlCommand command = CreateStoredProcedureCommand("_Sof_spSofTimeGetEntities", connection))
                {
                    connection.Open();

                    command.Parameters.AddWithValue("@act", "O");
                    command.Parameters.AddWithValue("@phrase", hint);

                    SqlDataAdapter adapter = new SqlDataAdapter(command);
                    adapter.Fill(dataset);

                    foreach (DataRow row in dataset.Tables[0].Rows)
                    {
                        row["acSubject"] = row["acSubject"].ToString().Trim();
                        row["acName2"]   = row["acName2"].ToString().Trim();
                    }

                    return dataset.Tables[0];
                }
            }
            catch (Exception)
            {
                throw;
            }
        }

        public DataTable GetSifrantiVrst(string hint = "")
        {
            DataSet dataset = new DataSet();
            try
            {
                using (SqlConnection connection = new SqlConnection(ConnectionString))
                using (SqlCommand command = CreateStoredProcedureCommand("_Sof_spSofTimeGetEntities", connection))
                {
                    connection.Open();

                    command.Parameters.AddWithValue("@act", "D");
                    command.Parameters.AddWithValue("@phrase", hint);

                    SqlDataAdapter adapter = new SqlDataAdapter(command);
                    adapter.Fill(dataset);
                    return dataset.Tables[0];
                }
            }
            catch (Exception)
            {
                throw;
            }
        }

        public bool DeleteWorktype(string id, int userid)
        {
            try
            {
                using (SqlConnection connection = new SqlConnection(ConnectionString))
                using (SqlCommand command = CreateStoredProcedureCommand("_Sof_spSofTimeManageEntities", connection))
                {
                    connection.Open();

                    command.Parameters.AddWithValue("@act", "D");
                    command.Parameters.AddWithValue("@acSetOf", id);
                    command.Parameters.AddWithValue("@anUserId", userid);

                    return command.ExecuteNonQuery() > 0;
                }
            }
            catch (Exception)
            {
                throw;
            }
        }

        private SqlCommand CreateStoredProcedureCommand(string spName, SqlConnection connection)
        {
            SqlCommand command = new SqlCommand(spName, connection);
            command.CommandType = System.Data.CommandType.StoredProcedure;

            return command;
        }

        public SqlConnectionStringBuilder GetConnectionStringBuilder()
        {
            return new SqlConnectionStringBuilder(ConnectionString);
        }
    }

    public class Opravek
    {
        public int? ID;
        public int UserID;
        public DateTime Zacetek;
        public string Subjekt;
        public string Vrsta;
        public bool Solved;
        public DateTime DateSolved;
        public string Department;
        public string Opis;
        public string Komentar;
        public string StroskovniNosilec;

        private TimeSpan _time;
        public int Time
        {
            get
            {
                return Convert.ToInt32(_time.TotalSeconds);
            }

            set
            {
                _time = TimeSpan.FromSeconds(value);
            }
        }

        public string SolvedString
        {
            get
            {
                return Solved ? "T" : "F";
            }
        }

        public Opravek()
        {
            this.ID = null;
        }
    }

    public class WorkTypeEntry
    {
        public string Identifier;
        public string pIdentifier;
        public string Name;
        public bool Mandatory;
        public string Article;
        public bool Chargeable;
        public List<WorkTypeEntry> Children;

        public WorkTypeEntry()
        {
            Children    = new List<WorkTypeEntry>();
        }

        public override string ToString()
        {
            return string.Format("{0} - {1}", Identifier, Name);
        }
    }

    public struct DatabaseEntry
    {
        public readonly string Name;
        public readonly string Company;

        public DatabaseEntry(string _Name, string _Company)
        {
            Name = _Name.Trim();
            Company = _Company.Trim();
        }

        public override string ToString()
        {
            return string.Format("{0} ({1})", Company.Trim(), Name.Trim());
        }
    }

    public struct UserEntry
    {
        public readonly string Username;
        public readonly string WebUsername;
        public readonly int Id;

        public UserEntry (string _Username, string _WebUsername, int id)
        {
            Username     = _Username;
            WebUsername  = _WebUsername;
            Id = id;
        }

        public override string  ToString()
        {
            return string.Format("{0} - {1}", Username, WebUsername);
        }
    }

    public enum UserType
    {
        Admin = 'A',
        User  = 'U',
        Unknown
    }
}
