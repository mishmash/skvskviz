﻿namespace Softech.Softime
{
    partial class MainWindow
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.components = new System.ComponentModel.Container();
            Telerik.WinControls.UI.GridViewTextBoxColumn gridViewTextBoxColumn1 = new Telerik.WinControls.UI.GridViewTextBoxColumn();
            Telerik.WinControls.UI.GridViewTextBoxColumn gridViewTextBoxColumn2 = new Telerik.WinControls.UI.GridViewTextBoxColumn();
            Telerik.WinControls.UI.GridViewDateTimeColumn gridViewDateTimeColumn1 = new Telerik.WinControls.UI.GridViewDateTimeColumn();
            Telerik.WinControls.UI.GridViewMultiComboBoxColumn gridViewMultiComboBoxColumn1 = new Telerik.WinControls.UI.GridViewMultiComboBoxColumn();
            Telerik.WinControls.UI.GridViewMultiComboBoxColumn gridViewMultiComboBoxColumn2 = new Telerik.WinControls.UI.GridViewMultiComboBoxColumn();
            Telerik.WinControls.UI.GridViewDateTimeColumn gridViewDateTimeColumn2 = new Telerik.WinControls.UI.GridViewDateTimeColumn();
            Telerik.WinControls.UI.GridViewTextBoxColumn gridViewTextBoxColumn3 = new Telerik.WinControls.UI.GridViewTextBoxColumn();
            Telerik.WinControls.UI.GridViewMultiComboBoxColumn gridViewMultiComboBoxColumn3 = new Telerik.WinControls.UI.GridViewMultiComboBoxColumn();
            Telerik.WinControls.UI.GridViewMultiComboBoxColumn gridViewMultiComboBoxColumn4 = new Telerik.WinControls.UI.GridViewMultiComboBoxColumn();
            Telerik.WinControls.UI.GridViewCheckBoxColumn gridViewCheckBoxColumn1 = new Telerik.WinControls.UI.GridViewCheckBoxColumn();
            Telerik.WinControls.UI.GridViewDateTimeColumn gridViewDateTimeColumn3 = new Telerik.WinControls.UI.GridViewDateTimeColumn();
            Telerik.WinControls.UI.GridViewTextBoxColumn gridViewTextBoxColumn4 = new Telerik.WinControls.UI.GridViewTextBoxColumn();
            Telerik.WinControls.Data.SortDescriptor sortDescriptor1 = new Telerik.WinControls.Data.SortDescriptor();
            System.ComponentModel.ComponentResourceManager resources = new System.ComponentModel.ComponentResourceManager(typeof(MainWindow));
            this.timerUra = new System.Windows.Forms.Timer(this.components);
            this.lblDatumOd = new System.Windows.Forms.Label();
            this.dtpDatumOd = new System.Windows.Forms.DateTimePicker();
            this.lblDatumDo = new System.Windows.Forms.Label();
            this.dtpDatumDo = new System.Windows.Forms.DateTimePicker();
            this.lblTrenutniCas = new System.Windows.Forms.Label();
            this.lblCurrentTime = new System.Windows.Forms.Label();
            this.pnlTop = new System.Windows.Forms.Panel();
            this.btnRefreshGrid = new System.Windows.Forms.Button();
            this.btnSaveEnty = new System.Windows.Forms.Button();
            this.btnDeleteEntrz = new System.Windows.Forms.Button();
            this.btnAddRow = new System.Windows.Forms.Button();
            this.cbUsers = new System.Windows.Forms.ComboBox();
            this.lblUporabnik = new System.Windows.Forms.Label();
            this.pnlMenuStrip = new System.Windows.Forms.Panel();
            this.menuStrip = new System.Windows.Forms.MenuStrip();
            this.operacijeToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.startCountTimeMenu = new System.Windows.Forms.ToolStripMenuItem();
            this.endCountTimeMenu = new System.Windows.Forms.ToolStripMenuItem();
            this.nastavitveToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.uporabnikiToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.pomočToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.oProgramuToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.izhodToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.radGridView1 = new Telerik.WinControls.UI.MasterGridViewTemplate();
            this.splitContainer1 = new System.Windows.Forms.SplitContainer();
            this.scMainContent = new System.Windows.Forms.SplitContainer();
            this.tvOpravila = new Telerik.WinControls.UI.RadTreeView();
            this.gvTasks = new Telerik.WinControls.UI.RadGridView();
            this.gridMenu = new System.Windows.Forms.ContextMenuStrip(this.components);
            this.miNovVnos = new System.Windows.Forms.ToolStripMenuItem();
            this.brišiVnosToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.shraniVnosToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.tbRemarks = new System.Windows.Forms.TextBox();
            this.miAddRow = new System.Windows.Forms.ToolStripMenuItem();
            this.treeMenu = new System.Windows.Forms.ContextMenuStrip(this.components);
            this.urejanjeToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.pnlTop.SuspendLayout();
            this.pnlMenuStrip.SuspendLayout();
            this.menuStrip.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.radGridView1)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.splitContainer1)).BeginInit();
            this.splitContainer1.Panel1.SuspendLayout();
            this.splitContainer1.Panel2.SuspendLayout();
            this.splitContainer1.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.scMainContent)).BeginInit();
            this.scMainContent.Panel1.SuspendLayout();
            this.scMainContent.Panel2.SuspendLayout();
            this.scMainContent.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.tvOpravila)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.gvTasks)).BeginInit();
            this.gridMenu.SuspendLayout();
            this.treeMenu.SuspendLayout();
            this.SuspendLayout();
            // 
            // lblDatumOd
            // 
            this.lblDatumOd.AutoSize = true;
            this.lblDatumOd.Font = new System.Drawing.Font("Microsoft Sans Serif", 9.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(238)));
            this.lblDatumOd.Location = new System.Drawing.Point(236, 19);
            this.lblDatumOd.Name = "lblDatumOd";
            this.lblDatumOd.Size = new System.Drawing.Size(69, 16);
            this.lblDatumOd.TabIndex = 0;
            this.lblDatumOd.Text = "Datum od:";
            // 
            // dtpDatumOd
            // 
            this.dtpDatumOd.CalendarFont = new System.Drawing.Font("Microsoft Sans Serif", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(238)));
            this.dtpDatumOd.Format = System.Windows.Forms.DateTimePickerFormat.Short;
            this.dtpDatumOd.Location = new System.Drawing.Point(311, 19);
            this.dtpDatumOd.Name = "dtpDatumOd";
            this.dtpDatumOd.Size = new System.Drawing.Size(200, 20);
            this.dtpDatumOd.TabIndex = 1;
            this.dtpDatumOd.ValueChanged += new System.EventHandler(this.selectionDate_ValueChanged);
            this.dtpDatumOd.KeyDown += new System.Windows.Forms.KeyEventHandler(this.dtpDatumOd_KeyDown);
            // 
            // lblDatumDo
            // 
            this.lblDatumDo.AutoSize = true;
            this.lblDatumDo.Font = new System.Drawing.Font("Microsoft Sans Serif", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(238)));
            this.lblDatumDo.Location = new System.Drawing.Point(534, 19);
            this.lblDatumDo.Name = "lblDatumDo";
            this.lblDatumDo.Size = new System.Drawing.Size(64, 15);
            this.lblDatumDo.TabIndex = 2;
            this.lblDatumDo.Text = "Datum do:";
            // 
            // dtpDatumDo
            // 
            this.dtpDatumDo.CalendarFont = new System.Drawing.Font("Microsoft Sans Serif", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(238)));
            this.dtpDatumDo.Format = System.Windows.Forms.DateTimePickerFormat.Short;
            this.dtpDatumDo.Location = new System.Drawing.Point(604, 19);
            this.dtpDatumDo.Name = "dtpDatumDo";
            this.dtpDatumDo.Size = new System.Drawing.Size(200, 20);
            this.dtpDatumDo.TabIndex = 3;
            this.dtpDatumDo.ValueChanged += new System.EventHandler(this.selectionDate_ValueChanged);
            this.dtpDatumDo.KeyDown += new System.Windows.Forms.KeyEventHandler(this.dtpDatumOd_KeyDown);
            // 
            // lblTrenutniCas
            // 
            this.lblTrenutniCas.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Right)));
            this.lblTrenutniCas.AutoSize = true;
            this.lblTrenutniCas.Location = new System.Drawing.Point(971, 22);
            this.lblTrenutniCas.Name = "lblTrenutniCas";
            this.lblTrenutniCas.Size = new System.Drawing.Size(73, 13);
            this.lblTrenutniCas.TabIndex = 4;
            this.lblTrenutniCas.Text = "Trenuten čas:";
            // 
            // lblCurrentTime
            // 
            this.lblCurrentTime.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Right)));
            this.lblCurrentTime.AutoSize = true;
            this.lblCurrentTime.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(238)));
            this.lblCurrentTime.Location = new System.Drawing.Point(1050, 22);
            this.lblCurrentTime.Name = "lblCurrentTime";
            this.lblCurrentTime.Size = new System.Drawing.Size(82, 13);
            this.lblCurrentTime.TabIndex = 5;
            this.lblCurrentTime.Text = "Trenuten čas";
            // 
            // pnlTop
            // 
            this.pnlTop.Controls.Add(this.btnRefreshGrid);
            this.pnlTop.Controls.Add(this.btnSaveEnty);
            this.pnlTop.Controls.Add(this.btnDeleteEntrz);
            this.pnlTop.Controls.Add(this.btnAddRow);
            this.pnlTop.Controls.Add(this.cbUsers);
            this.pnlTop.Controls.Add(this.lblUporabnik);
            this.pnlTop.Controls.Add(this.lblCurrentTime);
            this.pnlTop.Controls.Add(this.lblTrenutniCas);
            this.pnlTop.Controls.Add(this.dtpDatumDo);
            this.pnlTop.Controls.Add(this.lblDatumDo);
            this.pnlTop.Controls.Add(this.dtpDatumOd);
            this.pnlTop.Controls.Add(this.lblDatumOd);
            this.pnlTop.Dock = System.Windows.Forms.DockStyle.Top;
            this.pnlTop.Location = new System.Drawing.Point(0, 31);
            this.pnlTop.Name = "pnlTop";
            this.pnlTop.Size = new System.Drawing.Size(1144, 53);
            this.pnlTop.TabIndex = 0;
            // 
            // btnRefreshGrid
            // 
            this.btnRefreshGrid.BackgroundImage = global::Softech.Softime.Properties.Resources.table_refresh;
            this.btnRefreshGrid.BackgroundImageLayout = System.Windows.Forms.ImageLayout.Stretch;
            this.btnRefreshGrid.FlatAppearance.BorderSize = 0;
            this.btnRefreshGrid.FlatStyle = System.Windows.Forms.FlatStyle.Flat;
            this.btnRefreshGrid.Location = new System.Drawing.Point(884, 15);
            this.btnRefreshGrid.Name = "btnRefreshGrid";
            this.btnRefreshGrid.Size = new System.Drawing.Size(25, 25);
            this.btnRefreshGrid.TabIndex = 11;
            this.btnRefreshGrid.UseVisualStyleBackColor = true;
            this.btnRefreshGrid.Click += new System.EventHandler(this.btnRefreshGrid_Click);
            // 
            // btnSaveEnty
            // 
            this.btnSaveEnty.BackColor = System.Drawing.SystemColors.Control;
            this.btnSaveEnty.BackgroundImage = global::Softech.Softime.Properties.Resources.row_preferences;
            this.btnSaveEnty.BackgroundImageLayout = System.Windows.Forms.ImageLayout.Stretch;
            this.btnSaveEnty.Enabled = false;
            this.btnSaveEnty.FlatAppearance.BorderSize = 0;
            this.btnSaveEnty.FlatStyle = System.Windows.Forms.FlatStyle.Flat;
            this.btnSaveEnty.ForeColor = System.Drawing.SystemColors.ControlText;
            this.btnSaveEnty.Location = new System.Drawing.Point(915, 15);
            this.btnSaveEnty.Name = "btnSaveEnty";
            this.btnSaveEnty.Size = new System.Drawing.Size(25, 25);
            this.btnSaveEnty.TabIndex = 10;
            this.btnSaveEnty.UseVisualStyleBackColor = false;
            this.btnSaveEnty.Click += new System.EventHandler(this.btnSaveEnty_Click);
            // 
            // btnDeleteEntrz
            // 
            this.btnDeleteEntrz.BackgroundImage = global::Softech.Softime.Properties.Resources.row_delete;
            this.btnDeleteEntrz.BackgroundImageLayout = System.Windows.Forms.ImageLayout.Stretch;
            this.btnDeleteEntrz.FlatAppearance.BorderSize = 0;
            this.btnDeleteEntrz.FlatStyle = System.Windows.Forms.FlatStyle.Flat;
            this.btnDeleteEntrz.Location = new System.Drawing.Point(853, 15);
            this.btnDeleteEntrz.Name = "btnDeleteEntrz";
            this.btnDeleteEntrz.Size = new System.Drawing.Size(25, 25);
            this.btnDeleteEntrz.TabIndex = 9;
            this.btnDeleteEntrz.UseVisualStyleBackColor = true;
            this.btnDeleteEntrz.Click += new System.EventHandler(this.btnDeleteEntrz_Click);
            // 
            // btnAddRow
            // 
            this.btnAddRow.BackgroundImage = global::Softech.Softime.Properties.Resources.row_add;
            this.btnAddRow.BackgroundImageLayout = System.Windows.Forms.ImageLayout.Stretch;
            this.btnAddRow.FlatAppearance.BorderSize = 0;
            this.btnAddRow.FlatStyle = System.Windows.Forms.FlatStyle.Flat;
            this.btnAddRow.Location = new System.Drawing.Point(822, 16);
            this.btnAddRow.Name = "btnAddRow";
            this.btnAddRow.Size = new System.Drawing.Size(25, 25);
            this.btnAddRow.TabIndex = 8;
            this.btnAddRow.UseVisualStyleBackColor = true;
            this.btnAddRow.Click += new System.EventHandler(this.btnAddRow_Click);
            // 
            // cbUsers
            // 
            this.cbUsers.DropDownStyle = System.Windows.Forms.ComboBoxStyle.DropDownList;
            this.cbUsers.FormattingEnabled = true;
            this.cbUsers.Location = new System.Drawing.Point(78, 18);
            this.cbUsers.Name = "cbUsers";
            this.cbUsers.Size = new System.Drawing.Size(152, 21);
            this.cbUsers.TabIndex = 7;
            this.cbUsers.SelectedIndexChanged += new System.EventHandler(this.cbUsers_SelectedIndexChanged);
            // 
            // lblUporabnik
            // 
            this.lblUporabnik.AutoSize = true;
            this.lblUporabnik.Location = new System.Drawing.Point(13, 21);
            this.lblUporabnik.Name = "lblUporabnik";
            this.lblUporabnik.Size = new System.Drawing.Size(59, 13);
            this.lblUporabnik.TabIndex = 6;
            this.lblUporabnik.Text = "Uporabnik:";
            // 
            // pnlMenuStrip
            // 
            this.pnlMenuStrip.Controls.Add(this.menuStrip);
            this.pnlMenuStrip.Dock = System.Windows.Forms.DockStyle.Top;
            this.pnlMenuStrip.Location = new System.Drawing.Point(0, 0);
            this.pnlMenuStrip.Name = "pnlMenuStrip";
            this.pnlMenuStrip.Size = new System.Drawing.Size(1144, 31);
            this.pnlMenuStrip.TabIndex = 4;
            // 
            // menuStrip
            // 
            this.menuStrip.BackColor = System.Drawing.SystemColors.Control;
            this.menuStrip.Items.AddRange(new System.Windows.Forms.ToolStripItem[] {
            this.operacijeToolStripMenuItem,
            this.nastavitveToolStripMenuItem,
            this.pomočToolStripMenuItem});
            this.menuStrip.Location = new System.Drawing.Point(0, 0);
            this.menuStrip.Name = "menuStrip";
            this.menuStrip.Size = new System.Drawing.Size(1144, 24);
            this.menuStrip.TabIndex = 0;
            this.menuStrip.Text = "menuStrip1";
            // 
            // operacijeToolStripMenuItem
            // 
            this.operacijeToolStripMenuItem.DropDownItems.AddRange(new System.Windows.Forms.ToolStripItem[] {
            this.startCountTimeMenu,
            this.endCountTimeMenu});
            this.operacijeToolStripMenuItem.Name = "operacijeToolStripMenuItem";
            this.operacijeToolStripMenuItem.Size = new System.Drawing.Size(69, 20);
            this.operacijeToolStripMenuItem.Text = "Operacije";
            // 
            // startCountTimeMenu
            // 
            this.startCountTimeMenu.Name = "startCountTimeMenu";
            this.startCountTimeMenu.ShortcutKeys = System.Windows.Forms.Keys.F11;
            this.startCountTimeMenu.Size = new System.Drawing.Size(218, 22);
            this.startCountTimeMenu.Text = "Začetek beleženja časa";
            this.startCountTimeMenu.Click += new System.EventHandler(this.startCountTimeMenu_Click);
            // 
            // endCountTimeMenu
            // 
            this.endCountTimeMenu.Name = "endCountTimeMenu";
            this.endCountTimeMenu.ShortcutKeys = System.Windows.Forms.Keys.F12;
            this.endCountTimeMenu.Size = new System.Drawing.Size(218, 22);
            this.endCountTimeMenu.Text = "Konec beleženja časa";
            this.endCountTimeMenu.Click += new System.EventHandler(this.endCountTimeMenu_Click);
            // 
            // nastavitveToolStripMenuItem
            // 
            this.nastavitveToolStripMenuItem.DropDownItems.AddRange(new System.Windows.Forms.ToolStripItem[] {
            this.uporabnikiToolStripMenuItem});
            this.nastavitveToolStripMenuItem.Name = "nastavitveToolStripMenuItem";
            this.nastavitveToolStripMenuItem.Size = new System.Drawing.Size(74, 20);
            this.nastavitveToolStripMenuItem.Text = "Nastavitve";
            // 
            // uporabnikiToolStripMenuItem
            // 
            this.uporabnikiToolStripMenuItem.Name = "uporabnikiToolStripMenuItem";
            this.uporabnikiToolStripMenuItem.Size = new System.Drawing.Size(132, 22);
            this.uporabnikiToolStripMenuItem.Text = "Uporabniki";
            this.uporabnikiToolStripMenuItem.Click += new System.EventHandler(this.uporabnikiToolStripMenuItem_Click);
            // 
            // pomočToolStripMenuItem
            // 
            this.pomočToolStripMenuItem.DropDownItems.AddRange(new System.Windows.Forms.ToolStripItem[] {
            this.oProgramuToolStripMenuItem,
            this.izhodToolStripMenuItem});
            this.pomočToolStripMenuItem.Name = "pomočToolStripMenuItem";
            this.pomočToolStripMenuItem.Size = new System.Drawing.Size(57, 20);
            this.pomočToolStripMenuItem.Text = "Pomoč";
            // 
            // oProgramuToolStripMenuItem
            // 
            this.oProgramuToolStripMenuItem.Image = global::Softech.Softime.Properties.Resources.calendar;
            this.oProgramuToolStripMenuItem.Name = "oProgramuToolStripMenuItem";
            this.oProgramuToolStripMenuItem.Size = new System.Drawing.Size(139, 22);
            this.oProgramuToolStripMenuItem.Text = "O programu";
            this.oProgramuToolStripMenuItem.Click += new System.EventHandler(this.oProgramuToolStripMenuItem_Click);
            // 
            // izhodToolStripMenuItem
            // 
            this.izhodToolStripMenuItem.Name = "izhodToolStripMenuItem";
            this.izhodToolStripMenuItem.Size = new System.Drawing.Size(139, 22);
            this.izhodToolStripMenuItem.Text = "Izhod";
            this.izhodToolStripMenuItem.Click += new System.EventHandler(this.izhodToolStripMenuItem_Click);
            // 
            // splitContainer1
            // 
            this.splitContainer1.Dock = System.Windows.Forms.DockStyle.Fill;
            this.splitContainer1.Location = new System.Drawing.Point(0, 84);
            this.splitContainer1.Name = "splitContainer1";
            this.splitContainer1.Orientation = System.Windows.Forms.Orientation.Horizontal;
            // 
            // splitContainer1.Panel1
            // 
            this.splitContainer1.Panel1.Controls.Add(this.scMainContent);
            // 
            // splitContainer1.Panel2
            // 
            this.splitContainer1.Panel2.Controls.Add(this.tbRemarks);
            this.splitContainer1.Size = new System.Drawing.Size(1144, 430);
            this.splitContainer1.SplitterDistance = 332;
            this.splitContainer1.TabIndex = 5;
            // 
            // scMainContent
            // 
            this.scMainContent.Dock = System.Windows.Forms.DockStyle.Fill;
            this.scMainContent.Location = new System.Drawing.Point(0, 0);
            this.scMainContent.Name = "scMainContent";
            // 
            // scMainContent.Panel1
            // 
            this.scMainContent.Panel1.Controls.Add(this.tvOpravila);
            // 
            // scMainContent.Panel2
            // 
            this.scMainContent.Panel2.Controls.Add(this.gvTasks);
            this.scMainContent.Size = new System.Drawing.Size(1144, 332);
            this.scMainContent.SplitterDistance = 379;
            this.scMainContent.TabIndex = 4;
            // 
            // tvOpravila
            // 
            this.tvOpravila.Dock = System.Windows.Forms.DockStyle.Fill;
            this.tvOpravila.Location = new System.Drawing.Point(0, 0);
            this.tvOpravila.Name = "tvOpravila";
            this.tvOpravila.Size = new System.Drawing.Size(379, 332);
            this.tvOpravila.SpacingBetweenNodes = -1;
            this.tvOpravila.TabIndex = 1;
            this.tvOpravila.KeyDown += new System.Windows.Forms.KeyEventHandler(this.tvOpravila_KeyDown);
            this.tvOpravila.KeyUp += new System.Windows.Forms.KeyEventHandler(this.tvOpravila_KeyUp);
            this.tvOpravila.MouseDoubleClick += new System.Windows.Forms.MouseEventHandler(this.tvOpravila_MouseDoubleClick);
            // 
            // gvTasks
            // 
            this.gvTasks.AutoScroll = true;
            this.gvTasks.ContextMenuStrip = this.gridMenu;
            this.gvTasks.Dock = System.Windows.Forms.DockStyle.Fill;
            this.gvTasks.EnterKeyMode = Telerik.WinControls.UI.RadGridViewEnterKeyMode.EnterMovesToNextCell;
            this.gvTasks.Location = new System.Drawing.Point(0, 0);
            // 
            // gvTasks
            // 
            this.gvTasks.MasterTemplate.AllowAddNewRow = false;
            this.gvTasks.MasterTemplate.AllowDeleteRow = false;
            gridViewTextBoxColumn1.FieldName = "anId";
            gridViewTextBoxColumn1.HeaderText = "ID";
            gridViewTextBoxColumn1.Name = "idColumn";
            gridViewTextBoxColumn1.ReadOnly = true;
            gridViewTextBoxColumn1.Width = 25;
            gridViewTextBoxColumn2.FieldName = "acUser";
            gridViewTextBoxColumn2.HeaderText = "Uporabnik";
            gridViewTextBoxColumn2.IsVisible = false;
            gridViewTextBoxColumn2.Name = "userColumn";
            gridViewTextBoxColumn2.Width = 60;
            gridViewDateTimeColumn1.CustomFormat = "dd.MM.yyyy HH:mm";
            gridViewDateTimeColumn1.FieldName = "adBegin";
            gridViewDateTimeColumn1.Format = System.Windows.Forms.DateTimePickerFormat.Custom;
            gridViewDateTimeColumn1.FormatString = "{0:dd.MM.yyyy HH:mm}";
            gridViewDateTimeColumn1.HeaderText = "Začetek";
            gridViewDateTimeColumn1.Name = "beginColumn";
            gridViewDateTimeColumn1.SortOrder = Telerik.WinControls.UI.RadSortOrder.Descending;
            gridViewDateTimeColumn1.Width = 135;
            gridViewMultiComboBoxColumn1.DisplayMember = "acRealName";
            gridViewMultiComboBoxColumn1.FieldName = "acSetOf";
            gridViewMultiComboBoxColumn1.HeaderText = "Vrsta";
            gridViewMultiComboBoxColumn1.Name = "typeColumn";
            gridViewMultiComboBoxColumn1.ValueMember = "acSetOf";
            gridViewMultiComboBoxColumn2.FieldName = "acSubject";
            gridViewMultiComboBoxColumn2.HeaderText = "Subjekt";
            gridViewMultiComboBoxColumn2.Name = "subjectColumn";
            gridViewDateTimeColumn2.CustomFormat = "";
            gridViewDateTimeColumn2.EditorType = Telerik.WinControls.UI.GridViewDateTimeEditorType.TimePicker;
            gridViewDateTimeColumn2.FieldName = "anDateTime";
            gridViewDateTimeColumn2.Format = System.Windows.Forms.DateTimePickerFormat.Custom;
            gridViewDateTimeColumn2.FormatString = "{0:HH:mm:ss}";
            gridViewDateTimeColumn2.HeaderText = "Čas";
            gridViewDateTimeColumn2.Name = "timeColumn";
            gridViewTextBoxColumn3.FieldName = "acNote";
            gridViewTextBoxColumn3.HeaderText = "Opombe";
            gridViewTextBoxColumn3.IsVisible = false;
            gridViewTextBoxColumn3.Name = "remarkColumn";
            gridViewMultiComboBoxColumn3.FieldName = "acDept";
            gridViewMultiComboBoxColumn3.HeaderText = "Oddelek";
            gridViewMultiComboBoxColumn3.Name = "oddelekColumn";
            gridViewMultiComboBoxColumn4.FieldName = "acCostDrv";
            gridViewMultiComboBoxColumn4.HeaderText = "Strn";
            gridViewMultiComboBoxColumn4.Name = "stroskovniNosilecColumn";
            gridViewMultiComboBoxColumn4.ValueMember = "acCostDrv";
            gridViewCheckBoxColumn1.FieldName = "acIsSolved";
            gridViewCheckBoxColumn1.HeaderText = "Rešeno";
            gridViewCheckBoxColumn1.Name = "columnSolved";
            gridViewDateTimeColumn3.FieldName = "adDateOfSolution";
            gridViewDateTimeColumn3.Format = System.Windows.Forms.DateTimePickerFormat.Short;
            gridViewDateTimeColumn3.HeaderText = "Datum";
            gridViewDateTimeColumn3.Name = "dateColumn";
            gridViewTextBoxColumn4.FieldName = "acDescr";
            gridViewTextBoxColumn4.HeaderText = "Opis";
            gridViewTextBoxColumn4.Name = "descriptionColumn";
            this.gvTasks.MasterTemplate.Columns.AddRange(new Telerik.WinControls.UI.GridViewDataColumn[] {
            gridViewTextBoxColumn1,
            gridViewTextBoxColumn2,
            gridViewDateTimeColumn1,
            gridViewMultiComboBoxColumn1,
            gridViewMultiComboBoxColumn2,
            gridViewDateTimeColumn2,
            gridViewTextBoxColumn3,
            gridViewMultiComboBoxColumn3,
            gridViewMultiComboBoxColumn4,
            gridViewCheckBoxColumn1,
            gridViewDateTimeColumn3,
            gridViewTextBoxColumn4});
            sortDescriptor1.Direction = System.ComponentModel.ListSortDirection.Descending;
            sortDescriptor1.PropertyName = "beginColumn";
            this.gvTasks.MasterTemplate.SortDescriptors.AddRange(new Telerik.WinControls.Data.SortDescriptor[] {
            sortDescriptor1});
            this.gvTasks.Name = "gvTasks";
            this.gvTasks.Size = new System.Drawing.Size(761, 332);
            this.gvTasks.TabIndex = 2;
            this.gvTasks.CellBeginEdit += new Telerik.WinControls.UI.GridViewCellCancelEventHandler(this.gvTasks_CellBeginEdit);
            this.gvTasks.CellEditorInitialized += new Telerik.WinControls.UI.GridViewCellEventHandler(this.gvTasks_CellEditorInitialized);
            this.gvTasks.CurrentRowChanged += new Telerik.WinControls.UI.CurrentRowChangedEventHandler(this.gvTasks_CurrentRowChanged);
            this.gvTasks.SelectionChanged += new System.EventHandler(this.gvTasks_SelectionChanged);
            this.gvTasks.DefaultValuesNeeded += new Telerik.WinControls.UI.GridViewRowEventHandler(this.gvTasks_DefaultValuesNeeded);
            this.gvTasks.CellValueChanged += new Telerik.WinControls.UI.GridViewCellEventHandler(this.gvTasks_CellValueChanged);
            this.gvTasks.ColumnWidthChanged += new System.Windows.Forms.ColumnWidthChangedEventHandler(this.gvTasks_ColumnWidthChanged);
            this.gvTasks.ColumnWidthChanging += new System.Windows.Forms.ColumnWidthChangingEventHandler(this.gvTasks_ColumnWidthChanging);
            this.gvTasks.KeyDown += new System.Windows.Forms.KeyEventHandler(this.gvTasks_KeyDown);
            // 
            // gridMenu
            // 
            this.gridMenu.Items.AddRange(new System.Windows.Forms.ToolStripItem[] {
            this.miNovVnos,
            this.brišiVnosToolStripMenuItem,
            this.shraniVnosToolStripMenuItem});
            this.gridMenu.Name = "gridMenu";
            this.gridMenu.Size = new System.Drawing.Size(136, 70);
            // 
            // miNovVnos
            // 
            this.miNovVnos.Name = "miNovVnos";
            this.miNovVnos.Size = new System.Drawing.Size(135, 22);
            this.miNovVnos.Text = "Nov vnos";
            this.miNovVnos.Click += new System.EventHandler(this.miNovVnos_Click);
            // 
            // brišiVnosToolStripMenuItem
            // 
            this.brišiVnosToolStripMenuItem.Name = "brišiVnosToolStripMenuItem";
            this.brišiVnosToolStripMenuItem.Size = new System.Drawing.Size(135, 22);
            this.brišiVnosToolStripMenuItem.Text = "Briši vnos";
            this.brišiVnosToolStripMenuItem.Click += new System.EventHandler(this.brišiVnosToolStripMenuItem_Click);
            // 
            // shraniVnosToolStripMenuItem
            // 
            this.shraniVnosToolStripMenuItem.Name = "shraniVnosToolStripMenuItem";
            this.shraniVnosToolStripMenuItem.Size = new System.Drawing.Size(135, 22);
            this.shraniVnosToolStripMenuItem.Text = "Shrani vnos";
            this.shraniVnosToolStripMenuItem.Click += new System.EventHandler(this.shraniVnosToolStripMenuItem_Click);
            // 
            // tbRemarks
            // 
            this.tbRemarks.Dock = System.Windows.Forms.DockStyle.Fill;
            this.tbRemarks.Location = new System.Drawing.Point(0, 0);
            this.tbRemarks.Multiline = true;
            this.tbRemarks.Name = "tbRemarks";
            this.tbRemarks.Size = new System.Drawing.Size(1144, 94);
            this.tbRemarks.TabIndex = 0;
            this.tbRemarks.TextChanged += new System.EventHandler(this.tbRemarks_TextChanged);
            this.tbRemarks.KeyDown += new System.Windows.Forms.KeyEventHandler(this.tbRemarks_KeyDown);
            // 
            // miAddRow
            // 
            this.miAddRow.Name = "miAddRow";
            this.miAddRow.Size = new System.Drawing.Size(152, 22);
            this.miAddRow.Text = "Dodaj vnos";
            // 
            // treeMenu
            // 
            this.treeMenu.Items.AddRange(new System.Windows.Forms.ToolStripItem[] {
            this.urejanjeToolStripMenuItem});
            this.treeMenu.Name = "treeMenu";
            this.treeMenu.Size = new System.Drawing.Size(118, 26);
            this.treeMenu.Opening += new System.ComponentModel.CancelEventHandler(this.treeMenu_Opening);
            // 
            // urejanjeToolStripMenuItem
            // 
            this.urejanjeToolStripMenuItem.Name = "urejanjeToolStripMenuItem";
            this.urejanjeToolStripMenuItem.Size = new System.Drawing.Size(117, 22);
            this.urejanjeToolStripMenuItem.Text = "Urejanje";
            this.urejanjeToolStripMenuItem.Click += new System.EventHandler(this.urejanjeToolStripMenuItem_Click);
            // 
            // MainWindow
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.ClientSize = new System.Drawing.Size(1144, 514);
            this.Controls.Add(this.splitContainer1);
            this.Controls.Add(this.pnlTop);
            this.Controls.Add(this.pnlMenuStrip);
            this.Icon = ((System.Drawing.Icon)(resources.GetObject("$this.Icon")));
            this.MinimumSize = new System.Drawing.Size(1150, 550);
            this.Name = "MainWindow";
            this.StartPosition = System.Windows.Forms.FormStartPosition.CenterScreen;
            this.Text = "Form1";
            this.FormClosing += new System.Windows.Forms.FormClosingEventHandler(this.MainWindow_FormClosing);
            this.Load += new System.EventHandler(this.MainWindow_Load);
            this.LocationChanged += new System.EventHandler(this.MainWindow_LocationChanged);
            this.KeyDown += new System.Windows.Forms.KeyEventHandler(this.MainWindow_KeyDown);
            this.Resize += new System.EventHandler(this.MainWindow_Resize);
            this.pnlTop.ResumeLayout(false);
            this.pnlTop.PerformLayout();
            this.pnlMenuStrip.ResumeLayout(false);
            this.pnlMenuStrip.PerformLayout();
            this.menuStrip.ResumeLayout(false);
            this.menuStrip.PerformLayout();
            ((System.ComponentModel.ISupportInitialize)(this.radGridView1)).EndInit();
            this.splitContainer1.Panel1.ResumeLayout(false);
            this.splitContainer1.Panel2.ResumeLayout(false);
            this.splitContainer1.Panel2.PerformLayout();
            ((System.ComponentModel.ISupportInitialize)(this.splitContainer1)).EndInit();
            this.splitContainer1.ResumeLayout(false);
            this.scMainContent.Panel1.ResumeLayout(false);
            this.scMainContent.Panel2.ResumeLayout(false);
            ((System.ComponentModel.ISupportInitialize)(this.scMainContent)).EndInit();
            this.scMainContent.ResumeLayout(false);
            ((System.ComponentModel.ISupportInitialize)(this.tvOpravila)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.gvTasks)).EndInit();
            this.gridMenu.ResumeLayout(false);
            this.treeMenu.ResumeLayout(false);
            this.ResumeLayout(false);

        }

        #endregion

        private System.Windows.Forms.Timer timerUra;
        private System.Windows.Forms.Label lblDatumOd;
        private System.Windows.Forms.DateTimePicker dtpDatumOd;
        private System.Windows.Forms.Label lblDatumDo;
        private System.Windows.Forms.DateTimePicker dtpDatumDo;
        private System.Windows.Forms.Label lblTrenutniCas;
        private System.Windows.Forms.Label lblCurrentTime;
        private System.Windows.Forms.Panel pnlTop;
        private System.Windows.Forms.Panel pnlMenuStrip;
        private System.Windows.Forms.MenuStrip menuStrip;
        private System.Windows.Forms.ComboBox cbUsers;
        private System.Windows.Forms.Label lblUporabnik;
        private Telerik.WinControls.UI.MasterGridViewTemplate radGridView1;
        private System.Windows.Forms.SplitContainer splitContainer1;
        private System.Windows.Forms.SplitContainer scMainContent;
        private Telerik.WinControls.UI.RadTreeView tvOpravila;
        private System.Windows.Forms.TextBox tbRemarks;
        private Telerik.WinControls.UI.RadGridView gvTasks;
        private System.Windows.Forms.ContextMenuStrip gridMenu;
        private System.Windows.Forms.ToolStripMenuItem miAddRow;
        private System.Windows.Forms.ToolStripMenuItem pomočToolStripMenuItem;
        private System.Windows.Forms.ContextMenuStrip treeMenu;
        private System.Windows.Forms.ToolStripMenuItem miNovVnos;
        private System.Windows.Forms.ToolStripMenuItem brišiVnosToolStripMenuItem;
        private System.Windows.Forms.ToolStripMenuItem shraniVnosToolStripMenuItem;
        private System.Windows.Forms.ToolStripMenuItem urejanjeToolStripMenuItem;
        private System.Windows.Forms.ToolStripMenuItem operacijeToolStripMenuItem;
        private System.Windows.Forms.ToolStripMenuItem startCountTimeMenu;
        private System.Windows.Forms.ToolStripMenuItem endCountTimeMenu;
        private System.Windows.Forms.ToolStripMenuItem nastavitveToolStripMenuItem;
        private System.Windows.Forms.ToolStripMenuItem uporabnikiToolStripMenuItem;
        private System.Windows.Forms.ToolStripMenuItem oProgramuToolStripMenuItem;
        private System.Windows.Forms.ToolStripMenuItem izhodToolStripMenuItem;
        private System.Windows.Forms.Button btnAddRow;
        private System.Windows.Forms.Button btnDeleteEntrz;
        private System.Windows.Forms.Button btnRefreshGrid;
        private System.Windows.Forms.Button btnSaveEnty;
    }
}

