﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Windows.Forms;
using System.Reflection;
using System.Data.SqlClient;
using Telerik.WinControls.UI;
using System.Diagnostics;
using Telerik.WinControls.Data;
using Telerik.WinControls.UI.Localization;
using System.Data.SqlTypes;

namespace Softech.Softime
{
    public partial class MainWindow : Form
    {
        private Datalayer datalayer;
        private int UserId = -1;
        private UserType UserType;
        private GridViewRowInfo currentRow;
        private List<UserEntry> userList;
        private Timer vnosTimer;
        private Stopwatch vnosStopwatch;
        private List<string> errorList;
        private DateTime LastStarted;
        private int CurrentUserID = -1;

        private DatabaseEntry loginDatabaseEntry;
        public DatabaseEntry DatabaseEntry
        {
            set
            {
                loginDatabaseEntry = value;
            }

            get
            {
                return loginDatabaseEntry;
            }
        }

        private bool hasDefaultValues = false;

        GridViewMultiComboBoxColumn typeColumn;
        GridViewMultiComboBoxColumn subjectColumn;
        GridViewMultiComboBoxColumn oddelekColumn;
        public MainWindow()
        {
            InitializeComponent();

            // Da se takoj osvezi
            lblCurrentTime.Text = DateTime.UtcNow.ToString("HH:mm:ss");

            timerUra.Tick += new EventHandler(timerUra_Tick);
            timerUra.Interval = 1000;
            timerUra.Enabled  = true;
            timerUra.Start();

            gvTasks.MasterTemplate.AllowColumnHeaderContextMenu = false;
            gvTasks.MasterTemplate.AllowCellContextMenu = false;
            gvTasks.NewRowEnterKeyMode = RadGridViewNewRowEnterKeyMode.EnterMovesToNextCell;
            gvTasks.AutoSizeColumnsMode = GridViewAutoSizeColumnsMode.Fill;

            menuStrip.Renderer = new MenuStripRenderer();

            errorList = new List<string>();
            RadGridLocalizationProvider.CurrentProvider = new SiRadGridLocalizationProvider();
        }

        public MainWindow(DatabaseEntry database, int userId, UserType type)
            : this()
        {
            loginDatabaseEntry = database;

            var builder = new SqlConnectionStringBuilder(Properties.Settings.Default.ConnectionString);
            builder.InitialCatalog = database.Name;

            UserId    = userId;
            UserType  = type;
            datalayer = new Datalayer(builder.ConnectionString);

            Properties.Settings.Default.LastDb = database.Name;

            PrepareTaskContextMenu();
            PrepareTaskTypes();
            PrepareTaskTypeContextMenu();
            InitializeColumns();
            RestoreSettings();
            RefreshGrid();

            CurrentUserID = userId;
            if (UserType == UserType.User)
                cbUsers.Enabled = false;
        }

        private void MainWindow_Load(object sender, EventArgs e)
        {
            string database = string.Empty;
            if (datalayer.TestConnection())
            {
                database = datalayer.GetConnectionStringBuilder().InitialCatalog;
            }
            this.Text = string.Format("{0} ({1} ({2}))", Extensions.GetAssemblyTitle(), loginDatabaseEntry.Company, database);

            var users = datalayer.GetUsers();
            cbUsers.Items.Add(new UserEntry("VSI", "Vsi uporabniki", -1));
            foreach (UserEntry user in users)
            {
                cbUsers.Items.Add(user);
            }
            userList = users;
            cbUsers.SelectedItem = users.Where(u => u.Id == UserId).SingleOrDefault();

            btnSaveEnty.BackgroundImage = btnSaveEnty.BackgroundImage.SetImageOpacity(0.5f);

            Hotkey hotkeyIns = new Hotkey();
            hotkeyIns.KeyCode = Keys.Insert;
            hotkeyIns.Windows = false;

            hotkeyIns.Pressed += delegate (object snd, HandledEventArgs args)
            {
                miNovVnos_Click(this, new EventArgs());
            };
            hotkeyIns.Register(this);

            gvTasks.Focus();
        }

        private void InitializeColumns()
        {
            typeColumn = gvTasks.GetColumn<GridViewMultiComboBoxColumn>("typeColumn");
            typeColumn.DropDownStyle = Telerik.WinControls.RadDropDownStyle.DropDown;
            typeColumn.DataSource = datalayer.GetWorkTypesDt(true);

            subjectColumn = gvTasks.GetColumn<GridViewMultiComboBoxColumn>("subjectColumn");
            subjectColumn.DataSource = datalayer.GetSubjekti();

            oddelekColumn = gvTasks.GetColumn<GridViewMultiComboBoxColumn>("oddelekColumn");
            oddelekColumn.DataSource = datalayer.GetOddeleki();

            gvTasks.GetColumn<GridViewMultiComboBoxColumn>("stroskovniNosilecColumn").DataSource = datalayer.GetStroskovniNosilci();
        }

        private void MainWindow_FormClosing(object sender, FormClosingEventArgs e)
        {
            DialogResult result = MessageBox.Show("Ste prepričani da želite zapreti aplikacijo?", "Izhod", MessageBoxButtons.YesNo, MessageBoxIcon.Information);
            if (result == System.Windows.Forms.DialogResult.No)
                e.Cancel = true;

            Properties.Settings.Default.RestoreSettings = true;
            Properties.Settings.Default.Save();
        }

        private void timerUra_Tick(object sender, EventArgs e)
        {
            lblCurrentTime.Text = DateTime.Now.ToString("HH:mm:ss");
        }

        private void tbRemarks_KeyDown(object sender, KeyEventArgs e)
        {
            if (e.KeyData == (Keys.Control | Keys.A))
            {
                tbRemarks.SelectAll();
            }
        }
        
        private void PrepareTaskContextMenu()
        {
            ToolStripMenuItem columns = new ToolStripMenuItem("Stolpci");
            foreach (var column in gvTasks.MasterTemplate.Columns)
            {
                ToolStripMenuItem option = new ToolStripMenuItem(column.HeaderText);
                option.Click += new EventHandler(columnHide_Click);
                option.Checked = column.IsVisible;

                columns.DropDown.Items.Add(option);
            }
            gridMenu.Items.Add(columns);
        }

        private void PrepareTaskTypeContextMenu()
        {
            tvOpravila.ContextMenuStrip = treeMenu;
            ToolStripMenuItem delete = new ToolStripMenuItem("Briši");
            delete.Name = "deleteWorktype";
            delete.Click += new EventHandler(taskType_Delete);

            treeMenu.Items.Add(delete);
        }

        private void RefreshGrid()
        {
            DateTime from = dtpDatumOd.Value;
            DateTime to = dtpDatumDo.Value;

            DateTime start = new DateTime(from.Year, from.Month, from.Day, 0, 0, 0);
            DateTime end = new DateTime(to.Year, to.Month, to.Day, 23, 59, 59);

            gvTasks.DataSource = datalayer.GetOpravki(start, end, UserId);
        }

        private void RestoreSettings()
        {
            if (!Properties.Settings.Default.RestoreSettings)
                return;

            this.WindowState = Properties.Settings.Default.State;
            if (this.WindowState == FormWindowState.Minimized)
                this.WindowState = FormWindowState.Normal;
            this.Location    = Properties.Settings.Default.Location;

            if (this.WindowState == FormWindowState.Normal)
                this.Size = Properties.Settings.Default.Size;
        }

        private GridViewRowInfo AddRow(WorkTypeEntry entry = null)
        {
            GridViewRowInfo info = gvTasks.Rows.AddNew();

            var userCell = info.Cells["userColumn"];
            userCell.Value = userList.Where(u => u.Id == UserId).SingleOrDefault();

            var startCell = info.Cells["beginColumn"];
            startCell.Value = DateTime.Now;
            
            var timeColumn = info.Cells["timeColumn"];
            timeColumn.Value = new DateTime(0);

            var datumColumn = info.Cells["dateColumn"];
            datumColumn.Value = DateTime.Now;

            var typeColumn = info.Cells["typeColumn"];
            if (entry != null)
            {
                typeColumn.Value = entry.Identifier;
            }

            vnosTimer = new Timer();
            vnosTimer.Tick += new EventHandler(vnosTimer_Tick);
            vnosTimer.Interval = 1000;
            vnosTimer.Enabled = true;

            vnosTimer.Start();
            vnosStopwatch = new Stopwatch();
            vnosStopwatch.Start();

            LastStarted = DateTime.Now;

            hasDefaultValues = true;

            if (currentRow != null)
            {
                if (Validate(currentRow))
                    Save(currentRow);
            }

            gvTasks.CurrentRow = info;

            var tableElement = gvTasks.CurrentView as GridTableElement;
            if (tableElement != null && info != null)
                tableElement.ScrollToRow(info);
            return info;
        }

        private void vnosTimer_Tick(object sender, EventArgs e)
        {
            if (!gvTasks.Rows.Contains(currentRow))
                return;
            if (vnosStopwatch == null)
                return;

            var now = DateTime.Now;
            var diff = now.Subtract(LastStarted);

            var timeColumn = currentRow.Cells["timeColumn"];
            var timeValue =  (DateTime)timeColumn.Value;

            timeColumn.Value = new DateTime(timeValue.Ticks + diff.Ticks);
            gvTasks.Columns["timeColumn"].BestFit();

            LastStarted = DateTime.Now;
        }

        private void taskType_Delete(object sender, EventArgs e)
        {
            var selected = tvOpravila.SelectedNode;
            DialogResult result = MessageBox.Show(
                string.Format("Izbrišem vnos {0}?", selected.Name),
                "Brisanje vrste dela",
                MessageBoxButtons.YesNo,
                MessageBoxIcon.Question
            );
            if (result == System.Windows.Forms.DialogResult.Yes)
            {
                if (datalayer.DeleteWorktype(selected.Value.ToString(), UserId))
                {
                    tvOpravila.Nodes.Remove(selected.Name);
                    tvOpravila.Refresh();

                    typeColumn.DataSource = datalayer.GetWorkTypesDt(true);
                }
            }
        }

        private void columnHide_Click(object sender, EventArgs e)
        {
            var item = (ToolStripMenuItem)sender;
            item.Checked = !item.Checked;

            var column = gvTasks.Columns.Where(ic => ic.HeaderText == item.Text).SingleOrDefault();
            if (column != null)
            {
                column.IsVisible = item.Checked;
                column.BestFit();
            }
        }

        private void PrepareTaskTypes()
        {
            tvOpravila.Nodes.Clear();

            var types = datalayer.GetWorkTypes();
            AddTreeType(types);
        }

        private void AddTreeType(List<WorkTypeEntry> entries, RadTreeNode parent = null)
        {
            foreach (var item in entries)
            {
                RadTreeNode node = new RadTreeNode(item.ToString());
                node.Value = item.Identifier;

                if (parent == null)
                    tvOpravila.Nodes.Add(node);
                else
                    parent.Nodes.Add(node);
                
                AddTreeType(item.Children, node);
            }
        }

        private void Cleanup()
        {
            if (vnosTimer != null)
                vnosTimer.Stop();

            currentRow = null;
            return;
        }

        private void AddWithSelectedType()
        {
            if (!ValidateCurrent())
            {
                return;
            }

            var selected = tvOpravila.SelectedNode;
            if (selected != null)
            {
                WorkTypeEntry entry = new WorkTypeEntry();
                entry.Name = selected.Name;
                entry.Identifier = selected.Value.ToString();

                currentRow = AddRow(entry);
            }
        }

        private bool Save(GridViewRowInfo current)
        {
            bool isInsert = false;
            Opravek opravek = new Opravek();
            try
            {
                opravek.ID = int.Parse(current.Cells["idColumn"].Value.ToString());
            }
            catch (Exception)
            {
                isInsert = true;
            }

            try
            {
                opravek.Solved = bool.Parse(current.Cells["columnSolved"].Value.ToString());
            }
            catch (Exception)
            {
                opravek.Solved = false;
            }

            TimeSpan span;
            try
            {
                DateTime time = (DateTime)current.Cells["timeColumn"].Value;
                span = TimeSpan.FromTicks(time.Ticks);
            }
            catch (Exception)
            {
                span = TimeSpan.FromSeconds(0);
            }

            opravek.Subjekt = current.Cells["subjectColumn"].Value.ToString();
            opravek.Time = Convert.ToInt32(span.TotalSeconds);
            opravek.Vrsta = current.Cells["typeColumn"].Value.ToString();
            opravek.Zacetek = (DateTime)current.Cells["beginColumn"].Value;
            opravek.UserID = CurrentUserID;
            opravek.Department = current.Cells["oddelekColumn"].Value.ToString();
            opravek.Komentar = current.Cells["remarkColumn"].Value.ToString();
            opravek.Opis = current.Cells["descriptionColumn"].Value.ToString();
            opravek.DateSolved = (DateTime)current.Cells["dateColumn"].Value;
            opravek.StroskovniNosilec = current.Cells["stroskovniNosilecColumn"].Value.ToString();

            if (isInsert)
            {
                int opravekID = -1;
                if (datalayer.InsertOpravek(opravek, out opravekID))
                {
                    current.Cells["idColumn"].Value = opravekID;
                }
            }
            else
            {
                datalayer.UpdateOpravek(opravek);
            }
            return true;
        }

        private bool SaveCurrent()
        {
            if (gvTasks.Rows.Count == 0)
                return false;

            if (!ValidateCurrent())
                return false;

            var current = gvTasks.CurrentRow;
            if (current == null)
                return false;

            if (!hasDefaultValues)
            {
                return false;
            }

            if (!Save(current))
                return false;

            hasDefaultValues = false;
            return true;
        }

        private bool Validate(Opravek opravek)
        {
            return true;
        }

        private bool ValidateCurrent()
        {
            bool result = false;
            var current = gvTasks.CurrentRow;
            if (gvTasks.Rows.Count == 0)
                return true;

            if (current != null)
                result = Validate(current);
            return result;
        }

        private Dictionary<int, bool> alreadyWarned = new Dictionary<int, bool>();
        private bool Validate(GridViewRowInfo row)
        {
            bool validates = true;

            try
            {
                int _vt = row.Cells.Count;
            }
            catch (Exception)
            {
                return false;
            }

            var type    = row.Cells["typeColumn"].Value == null    ? string.Empty : row.Cells["typeColumn"].Value;
            var subject = row.Cells["subjectColumn"].Value == null ? string.Empty : row.Cells["subjectColumn"].Value;
            var date    = (DateTime)row.Cells["dateColumn"].Value;

            var types = datalayer.GetWorkTypes(true);
            var workt = types.Where(wt => wt.Identifier == type.ToString()).SingleOrDefault();

            GridViewCellInfo currentErrorCell = null;
            if (date != null)
            {
                try
                {
                    if (date < SqlDateTime.MinValue || date > SqlDateTime.MaxValue)
                    {
                        errorList.Add(string.Format("Datum mora biti med {0} in {1}", DateTime.MinValue.ToString(), DateTime.MaxValue.ToString()));
                        validates = false;
                        currentErrorCell = row.Cells["dateColumn"];
                    }
                }
                catch (Exception)
                {
                    errorList.Add(string.Format("Datum mora biti med {0} in {1}", DateTime.MinValue.ToString(), DateTime.MaxValue.ToString()));
                    validates = false;
                    currentErrorCell = row.Cells["dateColumn"];
                }
            }

            if (workt != null)
            {
                if (workt.Mandatory && subject.ToString().Equals(""))
                {
                    errorList.Add("Vnos subjekta je obvezen.");
                    validates = false;
                    currentErrorCell = row.Cells["typeColumn"];
                }
            }
            StringBuilder builder = new StringBuilder(errorList.Count);
            foreach (string str in errorList)
            {
                builder.Append("\t- " + str + "\n");
            }

            if (!validates)
                if (!alreadyWarned.ContainsKey(row.Index))
                {
                    MessageBox.Show(
                        "Prosim, popravite naslednje napake preden dodate nov vnos:\n" + builder.ToString(),
                        "Napaka",
                        MessageBoxButtons.OK,
                        MessageBoxIcon.Exclamation
                    );
                    alreadyWarned.Clear();
                }
            errorList.Clear();
            if (!validates)
            {
                try
                {
                    alreadyWarned.Add(row.Index, true);
                }
                catch (Exception) { }
                gvTasks.CurrentRow = row;
                if (currentErrorCell != null)
                    gvTasks.CurrentColumn = currentErrorCell.ColumnInfo;
            }
            return validates;
        }

        private void ToggleSaveButton(bool enabled)
        {
            if (enabled.Equals(false) && btnSaveEnty.Enabled)
            {
                btnSaveEnty.Enabled = false;
                btnSaveEnty.BackgroundImage = btnSaveEnty.BackgroundImage.SetImageOpacity(0.5f);
            }

            if (enabled.Equals(true) && !btnSaveEnty.Enabled)
            {
                btnSaveEnty.BackgroundImage = btnSaveEnty.BackgroundImage.SetImageOpacity(10.0f);
                btnSaveEnty.Enabled = true;
            }
        }

        private void DeleteRow(GridViewRowInfo row, bool justRemove = false)
        {
            if (row != null)
            {
                DialogResult result;
                if (!justRemove)
                    result = MessageBox.Show("Želite izbrisati vnos?", "Izbris", MessageBoxButtons.YesNo, MessageBoxIcon.Question);
                else
                    result = DialogResult.Yes;
                if (result == DialogResult.Yes)
                {
                    int id = -1;
                    try
                    {
                        id = int.Parse(row.Cells["idColumn"].Value.ToString());
                        if (justRemove)
                            return;
                        
                        if (datalayer.DeleteOpravek(id))
                        {
                            gvTasks.Rows.Remove(row);
                            Cleanup();
                        }
                    }
                    catch (Exception)
                    {
                        gvTasks.Rows.Remove(row);
                        Cleanup();
                        return;
                    }
                    finally
                    {
                        if (gvTasks.Rows.Count <= 1)
                            ToggleSaveButton(false);
                    }
                }
            }
        }

        private void oProgramuToolStripMenuItem_Click(object sender, EventArgs e)
        {
            AboutWindow about = new AboutWindow();
            about.ShowDialog();
        }

        private void treeMenu_Opening(object sender, CancelEventArgs e)
        {
            var selected = tvOpravila.SelectedNode;
            var item = treeMenu.Items.Find("deleteWorktype", false)[0];
            item.Enabled = true;
            if (selected == null)
                item.Enabled = false;
        }

        private void cbUsers_SelectedIndexChanged(object sender, EventArgs e)
        {
            ComboBox box   = sender as ComboBox;
            UserEntry user = (UserEntry)box.SelectedItem;

            if (UserId == user.Id)
                return;

            UserId = user.Id;
            RefreshGrid();
        }

        private void selectionDate_ValueChanged(object sender, EventArgs e)
        {
            RefreshGrid();
        }

        private void gvTasks_KeyDown(object sender, KeyEventArgs e)
        {
            if (e.KeyCode == Keys.Insert)
            {
                miNovVnos_Click(this, new EventArgs());
            }
            if (e.KeyCode == Keys.Delete)
            {
                var current = gvTasks.CurrentRow;
                if (current != null)
                    DeleteRow(current);
            }
            if (e.KeyCode == Keys.Escape)
            {
                DeleteRow(gvTasks.CurrentRow, true);
            }
            return;
        }

        private void gvTasks_CellValidating(object sender, Telerik.WinControls.UI.CellValidatingEventArgs e)
        {
        }

        private void gvTasks_RowValidating(object sender, RowValidatingEventArgs e)
        {
        }

        private void MainWindow_LocationChanged(object sender, EventArgs e)
        {
            if (this.WindowState == FormWindowState.Normal)
                Properties.Settings.Default.Location = this.Location;
        }

        private void MainWindow_Resize(object sender, EventArgs e)
        {
            Properties.Settings.Default.State = this.WindowState;
            if (this.WindowState == FormWindowState.Normal)
                Properties.Settings.Default.Size = this.Size;
        }

        private void miNovVnos_Click(object sender, EventArgs e)
        {
            if(vnosTimer != null) vnosTimer.Stop();

            if (!ValidateCurrent())
                return;

            currentRow = AddRow();
        }

        private void gvTasks_CellEditorInitialized(object sender, GridViewCellEventArgs e)
        {
            RadMultiColumnComboBoxElement el = e.ActiveEditor as RadMultiColumnComboBoxElement;
            if (el != null)
            {
                el.DropDownStyle = Telerik.WinControls.RadDropDownStyle.DropDown;
                if (gvTasks.Columns[e.ColumnIndex].Name == "oddelekColumn" ||
                    gvTasks.Columns[e.ColumnIndex].Name == "subjectColumn")
                {
                    el.AutoFilter = true;

                    FilterDescriptor filter = new FilterDescriptor();
                    filter.PropertyName = el.DisplayMember;
                    filter.Operator = FilterOperator.Contains;
                    el.EditorControl.MasterTemplate.FilterDescriptors.Add(filter);

                    el.Columns["acSubject"].HeaderText = "Subjekt";
                    el.Columns["acName2"].HeaderText = "Naziv";

                    if (gvTasks.Columns[e.ColumnIndex].Name == "oddelekColumn")
                    {
                        el.Columns["acName2"].IsVisible = false;
                    }
                }
                else if (gvTasks.Columns[e.ColumnIndex].Name == "typeColumn")
                {
                    el.Columns["acSetOf"].HeaderText = "ID";
                    el.Columns["acName"].HeaderText  = "Vrsta";
                    el.Columns["acIdent"].HeaderText = "Artikel";

                    el.AutoFilter = true;

                    FilterDescriptor filter = new FilterDescriptor();
                    filter.PropertyName     = el.DisplayMember;
                    filter.Operator         = FilterOperator.Contains;

                    el.EditorControl.MasterTemplate.FilterDescriptors.Add(filter);
                }
                else if (gvTasks.Columns[e.ColumnIndex].Name == "stroskovniNosilecColumn")
                {
                    el.Columns["acCostDrv"].HeaderText = "ID";
                    el.Columns["acName"].HeaderText    = "Naziv";
                    el.Columns["acActive"].HeaderText  = "Aktiven";
                }
            }
        }

        private void tvOpravila_MouseDoubleClick(object sender, MouseEventArgs e)
        {
            AddWithSelectedType();
        }

        private void gvTasks_CellBeginEdit(object sender, GridViewCellCancelEventArgs e)
        {
            if (gvTasks.CurrentColumn is GridViewMultiComboBoxColumn)
            {
                var editor = gvTasks.ActiveEditor as RadMultiColumnComboBoxElement;
                editor.AutoSizeDropDownToBestFit = true;
            }
        }

        private void gvTasks_CellValueChanged(object sender, GridViewCellEventArgs e)
        {
            if (gvTasks.Columns[e.ColumnIndex].Name == "remarkColumn")
            {
                string value = gvTasks.CurrentRow.Cells["remarkColumn"].Value.ToString();
                tbRemarks.Text = value;
            }
            ToggleSaveButton(true);
        }

        private void tbRemarks_TextChanged(object sender, EventArgs e)
        {
            var current = gvTasks.CurrentRow;
            if (current != null)
            {
                current.Cells["remarkColumn"].Value = tbRemarks.Text.ToString();
            }
        }

        private void brišiVnosToolStripMenuItem_Click(object sender, EventArgs e)
        {
            if (gvTasks.Rows.Count <= 0)
                return;

            var row = gvTasks.CurrentRow;
            DeleteRow(row);
        }

        private void urejanjeToolStripMenuItem_Click(object sender, EventArgs e)
        {
            WorkTypeWindow work = new WorkTypeWindow(datalayer, UserId);
            work.StartPosition = FormStartPosition.CenterScreen;
            work.ShowDialog();

            PrepareTaskTypes();
            typeColumn.DataSource = datalayer.GetWorkTypesDt(true);
        }

        bool isKeyDown = false;
        private void tvOpravila_KeyDown(object sender, KeyEventArgs e)
        {
            if (isKeyDown)
                return;

            if (e.KeyCode == Keys.Enter)
            {
                isKeyDown = true;
                AddWithSelectedType();
            }
        }

        private void tvOpravila_KeyUp(object sender, KeyEventArgs e)
        {
            isKeyDown = false;
        }

        private void shraniVnosToolStripMenuItem_Click(object sender, EventArgs e)
        {
            if (SaveCurrent())
            {
                if (vnosStopwatch != null) vnosStopwatch.Stop();
                if (vnosTimer != null) vnosTimer.Stop();
            }
        }

        private void uporabnikiToolStripMenuItem_Click(object sender, EventArgs e)
        {
            UserWindow user = new UserWindow();

            user.Datalayer = datalayer;
            user.UserID = UserId;
            user.AllowChangeUser = UserType == UserType.Admin;
            user.ShowDialog();
        }

        private void izhodToolStripMenuItem_Click(object sender, EventArgs e)
        {
            this.Close();
        }

        private void gvTasks_SelectionChanged(object sender, EventArgs e)
        {
            var current = gvTasks.CurrentRow;
            if (current == null)
                return;

            var text = current.Cells["remarkColumn"].Value;
            if (text != null)
                tbRemarks.Text = text.ToString();

            if (currentRow != null && Validate(currentRow))
            {
                if (Save(currentRow))
                    ToggleSaveButton(false);
            }
        }

        private void startCountTimeMenu_Click(object sender, EventArgs e)
        {
            var current = gvTasks.CurrentRow;
            if (current == null)
                return;

            currentRow = current;
            vnosStopwatch = new Stopwatch();
            if (vnosTimer == null)
            {
                vnosTimer = new Timer();
                vnosTimer.Tick += new EventHandler(vnosTimer_Tick);
                vnosTimer.Interval = 1000;
                vnosTimer.Enabled = true;
            }
            LastStarted = DateTime.Now;
            vnosTimer.Start();
            vnosStopwatch.Start();
        }

        private void endCountTimeMenu_Click(object sender, EventArgs e)
        {
            if (vnosStopwatch != null && vnosStopwatch.IsRunning)
                vnosStopwatch.Stop();
            if (vnosTimer != null)
                vnosTimer.Stop();
        }

        private void gvTasks_DefaultValuesNeeded(object sender, GridViewRowEventArgs e)
        {
        }

        private void btnRefreshGrid_Click(object sender, EventArgs e)
        {
            RefreshGrid();
        }

        private void btnDeleteEntrz_Click(object sender, EventArgs e)
        {
            if (gvTasks.Rows.Count <= 0)
                return;

            var current = gvTasks.CurrentRow;
            DeleteRow(current);
        }

        private void btnAddRow_Click(object sender, EventArgs e)
        {
            if (vnosTimer != null) vnosTimer.Stop();

            if (!ValidateCurrent())
                return;

            currentRow = AddRow();
        }

        private void btnSaveEnty_Click(object sender, EventArgs e)
        {
            if (gvTasks.CurrentRow == null)
                return;

            if (ValidateCurrent())
            {
                hasDefaultValues = true;
                if (SaveCurrent())
                {
                    if (vnosStopwatch != null) vnosStopwatch.Stop();
                    if (vnosTimer != null) vnosTimer.Stop();
                    ToggleSaveButton(false);
                }
            }
        }

        private void gvTasks_CurrentRowChanged(object sender, CurrentRowChangedEventArgs e)
        {
        }

        private void MainWindow_KeyDown(object sender, KeyEventArgs e)
        {
        }

        /**
         * Ce slucajno ne ustavimo timera se cell content updejta in vse zafuka.
         */
        bool stoppedByResize = false;
        private void gvTasks_ColumnWidthChanging(object sender, ColumnWidthChangingEventArgs e)
        {
            if (vnosTimer != null)
            {
                if (vnosTimer.Enabled)
                {
                    stoppedByResize = true;
                    vnosTimer.Stop();
                }
            }
        }

        private void gvTasks_ColumnWidthChanged(object sender, ColumnWidthChangedEventArgs e)
        {
            if (vnosTimer != null)
            {
                if (stoppedByResize)
                {
                    vnosTimer.Start();
                    stoppedByResize = false;
                }
            }
        }

        private void dtpDatumOd_KeyDown(object sender, KeyEventArgs e)
        {
            if (e.KeyCode == Keys.Delete)
            {
            }
        }
    }
}
