﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Windows.Forms;

namespace Softech.Softime
{
    public partial class LoginWindow : Form
    {
        private Datalayer datalayer = new Datalayer(Properties.Settings.Default.ConnectionString);
        private List<DatabaseEntry> entries = new List<DatabaseEntry>();

        public DatabaseEntry Database
        {
            get;
            set;
        }

        public int UserId
        {
            get;
            set;
        }

        public UserType UserType
        {
            get;
            set;
        }

        public LoginWindow()
        {
            InitializeComponent();
        }

        private void LoginWindow_Load(object sender, EventArgs e)
        {
            this.Text = string.Format("Prijava v {0}", Extensions.GetAssemblyTitle());
            this.AcceptButton = btnLogin;

            cbDatabase.Items.Add("Izberite bazo");
            foreach (DatabaseEntry entry in datalayer.GetDatabases())
            {
                cbDatabase.Items.Add(entry);
                entries.Add(entry);
            }
            cbDatabase.SelectedIndex = 0;

            if (Properties.Settings.Default.LastDb != string.Empty)
            {
                var selectedItem = entries.Where(ie => ie.Name == Properties.Settings.Default.LastDb).SingleOrDefault();
                cbDatabase.SelectedItem = selectedItem;
            }
        }

        private void btnLogin_Click(object sender, EventArgs e)
        {
            if (cbDatabase.SelectedIndex == 0)
            {
                MessageBox.Show("Prosim, izberite bazo", "Neveljavna baza", MessageBoxButtons.OK, MessageBoxIcon.Information);
                return;
            }

            int userId    = -1;
            UserType type = UserType.Unknown; 
            if (datalayer.Login(tbUsername.Text, tbPassword.Text, out userId, out type))
            {
                this.UserId   = userId;
                this.UserType = type;
 
                this.DialogResult = DialogResult.OK;
                this.Close();
            }
            else
            {
                MessageBox.Show("Podatki za prijavo so nepravilni", "Prijava ni uspela", MessageBoxButtons.OK, MessageBoxIcon.Exclamation);
            }
        }

        private void btnCancel_Click(object sender, EventArgs e)
        {
            this.Close();
        }

        private void LoginWindow_FormClosing(object sender, FormClosingEventArgs e)
        {
            if (this.DialogResult == System.Windows.Forms.DialogResult.OK)
                return;

            DialogResult result = MessageBox.Show("Želite zapreti aplikacijo?", "Zapiranje", MessageBoxButtons.YesNo, MessageBoxIcon.Information);
            if (result == DialogResult.No)
            {
                e.Cancel = true;
            }
        }

        private void cbDatabase_SelectedIndexChanged(object sender, EventArgs e)
        {
            ComboBox combo  = (ComboBox)sender;
            if (combo.SelectedIndex == 0)
            {
                tbPodjetje.Text = "";
                return;
            }

            DatabaseEntry dbName   = (DatabaseEntry)combo.SelectedItem;
            tbPodjetje.Text = entries.Where(de => de.Name == dbName.Name).SingleOrDefault().Company;
            this.Database = dbName;

            // Ce ze niso instalirane, instaliramo stored procedure zdaj
        RetryStoredProcedure:
            string previousDatabase = datalayer.GetCurrentDatabase();
            datalayer.SetCurrentDatabase(this.Database.Name);
            if (!Extensions.IsSpInstalled(datalayer.GetCurrentDatabase()) && !Extensions.InstallStoredProcedures(datalayer))
            {
                DialogResult result = MessageBox.Show("Inicializacija nekaterih SQL procedur ni uspela.", "Napaka", MessageBoxButtons.AbortRetryIgnore, MessageBoxIcon.Error);
                if (result == DialogResult.Abort)
                    return;
                else if (result == DialogResult.Retry)
                    goto RetryStoredProcedure;
            }
            else
            {
                Extensions.AddSpInstalled(datalayer.GetCurrentDatabase());
            }
        }
    }
}
